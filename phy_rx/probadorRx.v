module probadorRx( // modulo probadorDemux: generador de se�ales y monitor de datos para el demux 32 a 8
	output reg				clk_1MHz,
	output reg				clk_2MHz,
	output reg				clk_4MHz,
	output reg				clk_32MHz,
	output reg				reset,
	output reg				serial1,
	output reg				serial2);
	

	// codigo del probador
	initial begin
		$dumpfile("simulation.vcd");	// Nombre de archivo del "dump"
		$dumpvars;				// Directiva para "dumpear" variables

		// setear todo en 0
		serial1 <= 0;
		serial2 <= 0;
		reset <= 0;
		@(posedge clk_1MHz)
		
		reset <= 1;
		serial1 <= 'b0;
		serial2 <= 'b0;
		@(posedge clk_32MHz)
		
		repeat (1) @(posedge clk_32MHz);
		repeat(4)begin//Se inicia el envio de los BC
			repeat (2)begin
				@(posedge clk_32MHz)
				serial1 <= 0;
				serial2 <= 0;	
			end
			repeat (4)begin
				@(posedge clk_32MHz)
				serial1 <= 1;
				serial2 <= 1;	
			end
			@(posedge clk_32MHz) begin
				serial1 <= 0;
				serial2 <= 0;	
			end
			@(posedge clk_32MHz) begin
				serial1 <= 1;
				serial2 <= 1;	
			end
		end
		
		repeat (4) begin		// Repite 8 veces
			
			serial1 <= 1;	
			serial2 <= 1;		
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 0;
			serial2 <= 1;
			@(posedge clk_32MHz);
			
		end
		
		repeat (3) begin		// Repite 4 veces
			
			serial1 <= 1;
			serial2 <= 1;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 0;
			serial2 <= 1;
			@(posedge clk_32MHz);
			serial1 <= 1;
			serial2 <= 0;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 1;
			serial2 <= 0;
			@(posedge clk_32MHz);
			
		end
		
		repeat (4) begin		// Repite 4 veces
			
			serial1 <= 1;
			serial2 <= 0;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 0;
			serial2 <= 1;
			@(posedge clk_32MHz);
			serial1 <= 1;
			serial2 <= 1;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 1;
			serial2 <= 1;
			@(posedge clk_32MHz);
			serial1 <= 0;
			serial2 <= 1;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 1;
			serial2 <= 0;
			
		end
		
		repeat (4) begin		// Repite 4 veces
			
			serial1 <= 0;
			serial2 <= 1;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 1;
			serial2 <= 0;
			@(posedge clk_32MHz);
			serial1 <= 1;
			serial2 <= 0;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 1;
			serial2 <= 1;
			@(posedge clk_32MHz);
			serial1 <= 0;
			serial2 <= 0;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 1;
			serial2 <= 1;
			
		end
		
		repeat (4) begin		// Repite 4 veces
			
			serial1 <= 0;
			serial2 <= 1;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 0;
			serial2 <= 1;
			@(posedge clk_32MHz);
			serial1 <= 0;
			serial2 <= 0;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 1;
			serial2 <= 1;
			@(posedge clk_32MHz);
			serial1 <= 1;
			serial2 <= 0;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 0;
			serial2 <= 0;
			
		end
		
		
		
		// setear todo en 0
		serial1 <= 0;
		serial2 <= 0;
		reset <= 0;
		@(posedge clk_1MHz)
		
		reset <= 1;
		serial1 <= 'b0;
		serial2 <= 'b0;
		@(posedge clk_32MHz)
		
		repeat (1) @(posedge clk_32MHz);
		repeat(4)begin//Se inicia el envio de los BC solo en serial1
			repeat (2)begin
				@(posedge clk_32MHz)
				serial1 <= 0;
				serial2 <= 0;	
			end
			repeat (4)begin
				@(posedge clk_32MHz)
				serial1 <= 1;
				serial2 <= 1;	
			end
			@(posedge clk_32MHz) begin
				serial1 <= 0;
				serial2 <= 0;	
			end
			@(posedge clk_32MHz) begin
				serial1 <= 1;
				serial2 <= 1;	
			end
		end
		
		
		repeat (4) begin		// Repite 8 veces
			
			serial1 <= 1;	
			serial2 <= 0;		
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 0;
			serial2 <= 1;
			@(posedge clk_32MHz);
			
		end
		
		repeat (3) begin		// Repite 4 veces
			
			serial1 <= 0;
			serial2 <= 0;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 0;
			serial2 <= 1;
			@(posedge clk_32MHz);
			serial1 <= 1;
			serial2 <= 0;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 0;
			serial2 <= 1;
			@(posedge clk_32MHz);
			
		end
		
		repeat (4) begin		// Repite 4 veces
			
			serial1 <= 1;
			serial2 <= 0;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 0;
			serial2 <= 1;
			@(posedge clk_32MHz);
			serial1 <= 1;
			serial2 <= 1;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 1;
			serial2 <= 1;
			@(posedge clk_32MHz);
			serial1 <= 1;
			serial2 <= 0;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 0;
			serial2 <= 1;
			
		end
		
		repeat (4) begin		// Repite 4 veces
			
			serial1 <= 0;
			serial2 <= 1;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 1;
			serial2 <= 1;
			@(posedge clk_32MHz);
			serial1 <= 1;
			serial2 <= 0;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 1;
			serial2 <= 0;
			@(posedge clk_32MHz);
			serial1 <= 1;
			serial2 <= 1;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 0;
			serial2 <= 1;
			
		end
		
		repeat (4) begin		// Repite 4 veces
			
			serial1 <= 1;
			serial2 <= 1;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 0;
			serial2 <= 1;
			@(posedge clk_32MHz);
			serial1 <= 0;
			serial2 <= 1;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 0;
			serial2 <= 1;
			@(posedge clk_32MHz);
			serial1 <= 1;
			serial2 <= 0;
			@(posedge clk_32MHz);		// Repite 8 veces
			serial1 <= 0;
			serial2 <= 1;
			
		end
		
		
		
		$finish;			// Termina de almacenar se�ales
		//end
end
	// Reloj
	initial	begin
		clk_1MHz 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
		clk_2MHz 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
		clk_4MHz	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
		clk_32MHz	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	end
	always begin
		#1000 clk_1MHz 		<= ~clk_1MHz;		// Hace "toggle" cada 1000ns
	end
	always begin
		#500 clk_2MHz 		<= ~clk_2MHz;		// Hace "toggle" cada 500ns
	end
	always begin
		#250 clk_4MHz 		<= ~clk_4MHz;		// Hace "toggle" cada 250ns
	end
	always begin
		#31.25 clk_32MHz 	<= ~clk_32MHz;		// Hace "toggle" cada 31.25ns
	end
endmodule
