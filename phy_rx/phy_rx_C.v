`include "Serial_Paralelo_C.v"
`include "mux_8x32_C.v"
`include "mux_US_C.v"

module  phy_rx_C(		// comportamiento del demux 32x8
	input 					clk_1MHz,
	input					clk_2MHz,
	input					clk_4MHz,
	input					clk_32MHz,
	input 					reset,
	input 					serial1,
	input					serial2,
	output					activo0,
	output					activo1,
	output					validout,
	output[31:0]			USout);

wire spValid1, spValid2, muxValid1, muxValid2;
wire[7:0] paralelo1, paralelo2;
wire[31:0] muxOut1, muxOut2;

	// primero de serial a paralelo
	Serial_Paralelo_C SP1 (clk_32MHz, clk_4MHz, reset, serial1, spValid1, paralelo1,activo0);
	Serial_Paralelo_C SP2 (clk_32MHz, clk_4MHz, reset, serial2, spValid2, paralelo2,activo1);
	
	// NOTA: según serial->paralelo el valid empieza un ciclo adelantado, y eso puede causar problemas con la salida final del mux
	
	// segundo de paralelo a mux
	mux_8x32_C MUX1 (clk_4MHz, spValid1,reset,paralelo1,muxOut1,muxValid1);
	mux_8x32_C MUX2 (clk_4MHz, spValid2,reset,paralelo2,muxOut2,muxValid2);
	
	// NOTA: desaparació el valid
	
	// aplicar unstriping
	mux_US_C US (muxOut1, muxOut2, muxValid1, muxValid2, clk_4MHz, reset, validout, USout);


endmodule
