`timescale 	1ns				/ 100ps
// escala	unidad temporal (valor de "#1") / precisi�n
// includes de archivos de verilog
`include "probadorRx.v"
`include "cmos_cells.v"
`include "phy_rx_C.v"
`include "phy_rx_E.v"


module BancoPruebas; // Testbench
	// Declarar todo como wires
	wire clk_1MHz, clk_2MHz, clk_4MHz, clk_32MHz, serial1, serial2, reset, validout, activo0, activo1;
	wire[31:0] USout;
	
	// phy_rx_E: m�dulo estructural del phy receptor
	phy_rx_E		phy_rx_E(/*AUTOINST*/
					 // Outputs
					 .USout			(USout[31:0]),
					 .activo0		(activo0),
					 .activo1		(activo1),
					 .validout		(validout),
					 // Inputs
					 .clk_1MHz		(clk_1MHz),
					 .clk_2MHz		(clk_2MHz),
					 .clk_32MHz		(clk_32MHz),
					 .clk_4MHz		(clk_4MHz),
					 .reset			(reset),
					 .serial1		(serial1),
					 .serial2		(serial2));
	
	// phy_rx_C: m�dulo conductual del phy receptor
	phy_rx_C		phy_rx_C(/*AUTOINST*/
					 // Outputs
					 .activo0		(activo0),
					 .activo1		(activo1),
					 .validout		(validout),
					 .USout			(USout[31:0]),
					 // Inputs
					 .clk_1MHz		(clk_1MHz),
					 .clk_2MHz		(clk_2MHz),
					 .clk_4MHz		(clk_4MHz),
					 .clk_32MHz		(clk_32MHz),
					 .reset			(reset),
					 .serial1		(serial1),
					 .serial2		(serial2));

	
	// probador: describir el probador
	probadorRx		probadorRx(/*AUTOINST*/
					   // Outputs
					   .clk_1MHz		(clk_1MHz),
					   .clk_2MHz		(clk_2MHz),
					   .clk_4MHz		(clk_4MHz),
					   .clk_32MHz		(clk_32MHz),
					   .reset		(reset),
					   .serial1		(serial1),
					   .serial2		(serial2));
	
endmodule
