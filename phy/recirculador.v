module recirculador(
input  clk,
input [31:0]datos,
input active0,
input active1,
input valid_entrada,
input reset,

output reg [31:0] lane_active,
output reg [31:0] lane_probador,
output reg valid_salida0,
output reg valid_salida1);

reg active;

	always@(posedge clk) begin

		if (reset==0)begin

		
			valid_salida0	<= 0;
			valid_salida1	<= 0;
			lane_active 	<= 0;
			lane_probador 	<= 0;
			active		<= 0;
		end
		
		if (reset==1)begin
			
			active <= active0 & active1;
			if (active==1) begin
			valid_salida0<=valid_entrada;	//Valid hacia el phy
			  if(valid_entrada==1) 
			    lane_active<=datos; 		//Datos hacia el phy
			end

			if (active==0) begin
			  valid_salida0<=0;
		          lane_probador<=datos;
		          valid_salida1<=valid_entrada;
			end
		end
	end
endmodule
