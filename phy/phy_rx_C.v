`include "Serial_Paralelo_C.v"
`include "mux_8x32_C.v"
`include "mux_US_C.v"

module  phy_rx_C(		// comportamiento del demux 32x8
	input 					clk_1MHz,
	input					clk_2MHz,
	input					clk_4MHz,
	input					clk_32MHz,
	input 					reset_x,
	input 					serial1,
	input					serial2,
	output					activo0,
	output					activo1,
	output					validout,
	output[31:0]			Salida);

wire spValid1, spValid2, muxValid1, muxValid2;
wire[7:0] paralelo1, paralelo2;
wire[31:0] muxOut1, muxOut2;

	// primero de serial a paralelo
	Serial_Paralelo_C SP1 (.clk_32MHz(clk_32MHz),.clk_4MHz(clk_4MHz),.reset(reset_x),.serial_data(serial1),.valid_out(spValid1),
	 .serial_ParaleloOutput_C(paralelo1[7:0]),.active(activo0));
	Serial_Paralelo_C SP2 (.clk_32MHz(clk_32MHz),.clk_4MHz(clk_4MHz),.reset(reset_x),.serial_data(serial2),.valid_out(spValid2),
	 .serial_ParaleloOutput_C(paralelo2[7:0]),.active(activo1));
	
	// NOTA: según serial->paralelo el valid empieza un ciclo adelantado, y eso puede causar problemas con la salida final del mux
	
	// segundo de paralelo a mux
	mux_8x32_C MUX1 (.clk_4MHz(clk_4MHz), .validIn(spValid1),.reset(reset_x),.muxInput(paralelo1[7:0]),.salidaMux_C(muxOut1[31:0]),
	.validOut(muxValid1));
	mux_8x32_C MUX2 (.clk_4MHz(clk_4MHz), .validIn(spValid2),.reset(reset_x),.muxInput(paralelo2[7:0]),.salidaMux_C(muxOut2[31:0]),
	.validOut(muxValid2));
	
	// NOTA: desaparació el valid
	
	// aplicar unstriping
	mux_US_C US (.lineIN0(muxOut1[31:0]),.lineIN1(muxOut2[31:0]), .valid0(muxValid1), .valid1(muxValid2),
	.clk(clk_2MHz), .reset(reset_x), .validout(validout), .data_out(Salida));


endmodule
