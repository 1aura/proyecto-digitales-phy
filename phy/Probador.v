module Probador(
output  reg clk_2MHz,
output  reg clk_4MHz,
output  reg clk_1MHz,
output  reg clk_32MHz,
output 	reg [31:0] Entrada,
output 	reg  reset_x,
output 	reg  valido,
output 	reg [31:0] Entrada_E,
output 	reg valido_E
);


    
initial begin
    $dumpfile("simulation.vcd");
    $dumpvars;
   
       
		Entrada<= 31'h0;
		valido <= 0;
		valido_E<= 0;
		Entrada_E<= 31'h0;
		reset_x<= 0;
		

	@(posedge clk_2MHz);


	repeat (8) begin		// Repite 8 veces

		valido <= 1;
		valido_E <= 1;
		Entrada<= Entrada + 1901;
		Entrada_E<=Entrada_E+ 1901;
		reset_x<= 1;
			end
	repeat (8) begin		// Repite 8 veces
		valido_E <= 1;
		valido <= 1;
		Entrada<= Entrada + 7901;
		Entrada_E<=Entrada_E+7901;
		reset_x<= 1;

	end
		@(posedge clk_2MHz);
		
		reset_x <= 1;
		valido <= 'b1;
		Entrada <= Entrada + 4132;
		Entrada_E<=Entrada_E+4132;
		valido_E <= 1;
	@(posedge clk_2MHz);
	repeat (8) begin		// Repite 8 veces
		valido_E <= 1;
		valido <= 1;
		Entrada<= Entrada + 6901;
		Entrada_E<=Entrada_E+6901;
		reset_x<= 1;
			end		
		
		@(posedge clk_2MHz);
		repeat (8) begin		// Repite 8 veces
		valido_E <= 1;
		valido <= 1;
		Entrada<= Entrada + 101;
		Entrada_E<=Entrada_E+101;
		reset_x<= 1;
			end

		@(posedge clk_2MHz);
		repeat (8) begin		// Repite 8 veces
		valido_E <= 1;
		valido <= 1;
		Entrada<= Entrada + 9971;
		Entrada_E<=Entrada_E+9971;
		reset_x<= 1;
			end


		@(posedge clk_2MHz);
		repeat (8) begin		// Repite 8 veces
		valido_E <= 1;
		valido <= 1;
		Entrada<= Entrada + 3901;
		Entrada_E<=Entrada_E+3901;
		reset_x<= 1;
			end

		@(posedge clk_2MHz);
		repeat (8) begin		// Repite 8 veces
		valido_E <= 1;
		valido <= 1;
		Entrada<= Entrada + 1901;
		Entrada_E<=Entrada_E+1901;
		reset_x<= 1;
			end

		@(posedge clk_2MHz);
		repeat (8) begin		// Repite 8 veces
		valido_E <= 1;
		valido <= 1;
		Entrada<= Entrada + 6901;
		Entrada_E<=Entrada_E+6901;
		reset_x<= 1;
			end

		@(posedge clk_2MHz);
		repeat (8) begin		// Repite 8 veces
		valido_E <= 1;
		valido <= 1;
		Entrada<= Entrada + 5910;
		Entrada_E<=Entrada_E+5910;
		reset_x<= 1;
			end

		@(posedge clk_2MHz);
		repeat (8) begin		// Repite 8 veces
		valido_E <= 1;
		valido <= 1;
		Entrada<= Entrada + 501;
		Entrada_E<=Entrada_E+501;
		reset_x<= 1;
			end


		@(posedge clk_2MHz);
		repeat (8) begin		// Repite 8 veces
		valido_E <= 1;
		valido <= 1;
		Entrada<= Entrada + 501;
		Entrada_E<=Entrada_E+501;
		reset_x<= 1;
			end

		@(posedge clk_2MHz);
		repeat (8) begin		// Repite 8 veces
		valido_E <= 1;
		valido <= 1;
		Entrada<= Entrada + 9501;
		Entrada_E<=Entrada_E + 9501;
		reset_x<= 1;
			end

		@(posedge clk_2MHz);
		repeat (8) begin		// Repite 8 veces
		valido_E <= 1;
		valido <= 1;
		Entrada<= Entrada + 1501;
		Entrada_E<=Entrada_E + 1501;
		reset_x<= 1;
			end

		@(posedge clk_2MHz);
		repeat (8) begin		// Repite 8 veces
		valido_E <= 1;
		valido <= 1;
		Entrada<= Entrada + 3501;
		Entrada_E<=Entrada_E + 3501;
		reset_x<= 1;
			end


		@(posedge clk_2MHz);
		repeat (8) begin		// Repite 8 veces
		valido_E <= 1;
		valido <= 1;
		Entrada<= Entrada + 8501;
		Entrada_E<=Entrada_E + 8501;
		reset_x<= 1;
		end
		
		@(posedge clk_2MHz);
		repeat (8) begin		// Repite 8 veces
		valido_E <= 1;
		valido <= 1;
		Entrada<= Entrada + 8501;
		Entrada_E<=Entrada_E + 8501;
		reset_x<= 1;
		end
		
		
		repeat (15) begin
		@(posedge clk_2MHz);
		repeat (8) begin		// Repite 8 veces
		valido_E <= 1;
		valido <= 0;
		Entrada<= Entrada + 8501;
		Entrada_E<=Entrada_E + 8501;
		reset_x<= 1;
		end
		end
		
		repeat (25) begin
		@(posedge clk_2MHz);
		repeat (8) begin		// Repite 8 veces
		valido_E <= 1;
		valido <= valido + 1;
		Entrada<= Entrada + 8501;
		Entrada_E<=Entrada_E + 8501;
		end
		end
		

		



       $finish;
            end
        initial	begin
		clk_1MHz	<= 0;
		clk_2MHz 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	    clk_4MHz	<= 0;
		clk_32MHz	<= 0;

		end	

// Reloj
	always begin
		#1000 clk_1MHz 		<= ~clk_1MHz;		// Hace "toggle" cada 1000ns
	end
	always begin
		#500 clk_2MHz 		<= ~clk_2MHz;		// Hace "toggle" cada 500ns
	end
	always begin
		#250 clk_4MHz 		<= ~clk_4MHz;		// Hace "toggle" cada 250ns
	end
	always begin
		#31.25 clk_32MHz 	<= ~clk_32MHz;		// Hace "toggle" cada 31.25ns
	end

	//always @(posedge clk_2MHz)begin;
	//repeat(1) @(posedge clk_2MHz);	
	//	valido <= ~ valido;	    
	//end
endmodule
