`timescale 	1ns				/ 100ps
// escala	unidad temporal (valor de "#1") / precisi�n
// includes de archivos de verilog
`include "Probador.v"
`include "PHY_E.v"
`include "PHY_C.v"
`include "cmos_cells.v"


module BancoPruebas; // Testbench
	// Declarar todo como wires
	wire reset_x,clk_1MHz, clk_2MHz, clk_4MHz, clk_32MHz, valido, valido_C,valido_E,valido_out_E, valid_out;
	wire[31:0] Entrada, Entrada_E, Salida,Salida_E;
	
	

	
	// phy_C: m�dulo estructural del phy
	PHY_C		PHY_C(/*AUTOINST*/
			      // Outputs
			      .validout		(validout),
			      .Salida		(Salida[31:0]),
			      // Inputs
			      .clk_1MHz		(clk_1MHz),
			      .clk_2MHz		(clk_2MHz),
			      .clk_4MHz		(clk_4MHz),
			      .clk_32MHz	(clk_32MHz),
			      .Entrada		(Entrada[31:0]),
			      .reset_x		(reset_x),
			      .valido		(valido));


	PHY_E		PHY_E(/*AUTOINST*/
			      // Outputs
			      .validout		(validout),
			      .Salida		(Salida_E[31:0]),
			      // Inputs
			      .clk_1MHz		(clk_1MHz),
			      .clk_2MHz		(clk_2MHz),
			      .clk_4MHz		(clk_4MHz),
			      .clk_32MHz	(clk_32MHz),
			      .Entrada		(Entrada_E[31:0]),
			      .reset_x		(reset_x),
			      .valido		(valido));

	
	// probador: describir el probador
	Probador		Probador(/*AUTOINST*/
					 // Outputs
					 .clk_2MHz		(clk_2MHz),
					 .clk_4MHz		(clk_4MHz),
					 .clk_1MHz		(clk_1MHz),
					 .clk_32MHz		(clk_32MHz),
					 .Entrada		(Entrada[31:0]),
					 .reset_x		(reset_x),
					 .valido		(valido),
					 .Entrada_E		(Entrada_E[31:0]),
					 .valido_E		(valido_E));
	
endmodule
