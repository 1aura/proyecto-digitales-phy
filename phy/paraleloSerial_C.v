module paraleloSerial_C(	// comportamiento del demux 32x8
	input 				clk_32MHz,
	input				validIn,
	input 				reset,
	input [7:0] 		parallelInput,
	output reg 			serialOutput_C);
					

reg completeWord;
reg[2:0] selector, counter, invalidCount; 
reg[7:0] temp,saved,backup;

//Revisar entradas en cada flanco positivo del reloj
always @(posedge clk_32MHz) begin // DEMUX code

	if (reset == 0) begin
		serialOutput_C <= 'b0;
		selector <= 0;
		temp <= 'b10111100;
		saved <= 'b10111100;
		backup <= 'b10111100;
		counter <= 0;
		invalidCount <= 0;
		completeWord <= 0;
	end
	else begin
	
		if (validIn == 0) begin
			counter <= 0;
			invalidCount <= invalidCount + 1;
			saved <= 'b10111100;
			if (invalidCount == 'b111) begin
				completeWord <= 1;
			end
		end
		else begin
			counter <= counter + 1;
			invalidCount <= 0;
		end
		
		
		selector <= selector + 1;
		case(selector)
			3'b000: serialOutput_C <= temp[0];        
			3'b001: serialOutput_C <= temp[1];                
			3'b010: serialOutput_C <= temp[2];                
			3'b011: serialOutput_C <= temp[3];  
			3'b100: serialOutput_C <= temp[4];        
			3'b101: serialOutput_C <= temp[5];                
			3'b110: serialOutput_C <= temp[6];                
			3'b111: serialOutput_C <= temp[7];                       
		endcase   

		case(counter)
			3'b000: saved[0] <= parallelInput[0];        
			3'b001: saved[1] <= parallelInput[1];                
			3'b010: saved[2] <= parallelInput[2];                
			3'b011: saved[3] <= parallelInput[3];  
			3'b100: saved[4] <= parallelInput[4];        
			3'b101: saved[5] <= parallelInput[5];                
			3'b110: saved[6] <= parallelInput[6];                
			3'b111: saved[7] <= parallelInput[7];                       
		endcase   
	end
	
	
	if (counter == 'b111) begin
		completeWord <= 1;
	end
	
	if (completeWord == 1) begin
		backup <= saved;
		completeWord <= 0;
	end
	
	if (selector == 'b110) begin
		temp <= backup;
	end
	
end

endmodule
