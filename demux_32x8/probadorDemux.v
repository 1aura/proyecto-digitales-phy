module probadorDemux( // modulo probadorDemux: generador de se�ales y monitor de datos para el demux 32 a 8
	output reg				clk_1MHz, //clock es especial
	output reg 				clk_2MHz,
	output reg 				clk_4MHz,
	output reg				clk_32MHz,
	output reg 				validIn,
	output reg 				reset,
	input 					validOut,
	input [7:0]				salidaDemux_C,
	input [7:0]				salidaDemux_E,
	output reg [31:0]		demuxInput);
	

	// codigo del probador
	initial begin
		$dumpfile("simulation.vcd");	// Nombre de archivo del "dump"
		$dumpvars;				// Directiva para "dumpear" variables

		// 0
		validIn = 'b0;
		reset = 0;
		demuxInput = 'b0000000000000000000000000000000;
		@(posedge clk_1MHz)
		
		repeat (8) begin		// Repite 8 veces
			
			reset <= 1;
			validIn <= 'b1;
			demuxInput <= demuxInput + 51153;
			
			@(posedge clk_1MHz);		// Repite 8 veces
			end
		
		reset <= 1;
		validIn <= 'b0;
		demuxInput <= demuxInput + 44132;
		@(posedge clk_1MHz);
		
		reset <= 0;
		validIn <= 'b1;
		demuxInput <= demuxInput + 44132;
		@(posedge clk_1MHz);
		
		repeat (8) begin		// Repite 8 veces
			
			reset <= 1;
			validIn <= 'b1;
			demuxInput <= demuxInput + 32132;
			
			@(posedge clk_1MHz);		// Repite 8 veces
			end		

		$finish;			// Termina de almacenar se�ales
		//end
end
	initial	begin
		clk_1MHz 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
		clk_2MHz 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
		clk_4MHz	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
		clk_32MHz	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	end
	always begin
		#1000 clk_1MHz 		<= ~clk_1MHz;		// Hace "toggle" cada 1000ns
	end
	always begin
		#500 clk_2MHz 		<= ~clk_2MHz;		// Hace "toggle" cada 500ns
	end
	always begin
		#250 clk_4MHz 		<= ~clk_4MHz;		// Hace "toggle" cada 250ns
	end
	always begin
		#31.25 clk_32MHz 	<= ~clk_32MHz;		// Hace "toggle" cada 31.25ns
	end
endmodule
