module demux_32x8_C(	// comportamiento del demux 32x8
	input 				clk_4MHz,
	input				validIn,
	input 				reset,
	input [31:0] 		demuxInput,
	output reg			validOut,
	output reg [7:0] 	salidaDemux_C);
					

reg[1:0] selector; 

//Revisar entradas en cada flanco positivo del reloj
always @(posedge clk_4MHz) begin // DEMUX code

	if (reset == 0) begin
		salidaDemux_C <= 'b00000000;
		selector <= 0;
		validOut <= 0;
	end
	else if (validIn == 0) begin
		salidaDemux_C <= 'b00000000;
		selector <= 0;
		validOut <= 0;
	end
	else begin	
		selector <= selector + 1;
		case(selector)
			2'b11: salidaDemux_C <= demuxInput[7:0];        
			2'b10: salidaDemux_C <= demuxInput[15:8];                
			2'b01: salidaDemux_C <= demuxInput[23:16];                
			2'b00: salidaDemux_C <= demuxInput[31:24];                       
		endcase  
		validOut <= 1; 
	end
	
	
end

endmodule
