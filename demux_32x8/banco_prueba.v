`timescale 	1ns				/ 100ps
// escala	unidad temporal (valor de "#1") / precisión
// includes de archivos de verilog
`include "probadorDemux.v"
`include "demux_32x8_E.v"
`include "demux_32x8_C.v"
`include "cmos_cells.v"


module BancoPruebas; // Testbench
	// Declarar todo como wires
	wire clk_1MHz, clk_2MHz, clk_4MHz, clk_32MHz, validOut, validIn, reset;
	wire[7:0] salidaDemux_C, salidaDemux_E;
	wire[31:0] demuxInput;
	
	// demux conductual
	demux_32x8_C		demux_32x8_C(/*AUTOINST*/
					     // Outputs
					     .validOut		(validOut),
					     .salidaDemux_C	(salidaDemux_C[7:0]),
					     // Inputs
					     .clk_4MHz		(clk_4MHz),
					     .validIn		(validIn),
					     .reset		(reset),
					     .demuxInput	(demuxInput[31:0]));
					     
	// demux estructural
	demux_32x8_E		demux_32x8_E(/*AUTOINST*/
					     // Outputs
					     .salidaDemux_E	(salidaDemux_E[7:0]),
					     .validOut		(validOut),
					     // Inputs
					     .clk_4MHz		(clk_4MHz),
					     .demuxInput	(demuxInput[31:0]),
					     .reset		(reset),
					     .validIn		(validIn));
	
	// probador: describir el probador
	probadorDemux		probadorDemux(/*AUTOINST*/
					      // Outputs
					      .clk_1MHz		(clk_1MHz),
					      .clk_2MHz		(clk_2MHz),
					      .clk_4MHz		(clk_4MHz),
					      .clk_32MHz	(clk_32MHz),
					      .validIn		(validIn),
					      .reset		(reset),
					      .demuxInput	(demuxInput[31:0]),
					      // Inputs
					      .validOut		(validOut),
					      .salidaDemux_C	(salidaDemux_C[7:0]),
					      .salidaDemux_E	(salidaDemux_E[7:0]));
	
endmodule
