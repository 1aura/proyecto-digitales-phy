`timescale 	1ns				/ 100ps
// escala	unidad temporal (valor de "#1") / precisi�n
// includes de archivos de verilog
// Pueden omitirse y llamarse desde el testbench
`include "mux_US_C.v"
`include "mux_US_E.v"
`include "cmos_cells.v"
`include "probador.v"

module BancoPruebas; // Testbench
	// Por lo general, las se�ales en el banco de pruebas son wires.
	// No almacenan un valor, son manejadas por otras instancias de m�dulos.
	wire [31:0] data_out, data_outE, lineIN0, lineIN1;
	wire clk;


	// Probador: generador de se�ales y monitor
	probador probador_(/*AUTOINST*/
			   // Outputs
			   .clk_1MHz		(clk_1MHz),
			   .clk_4MHz		(clk_4MHz),
			   .clk_32MHz		(clk_32MHz),
			   .lineIN0		(lineIN0[31:0]),
			   .serial_data		(serial_data),
			   .valid0		(valid0),
			   .valid1		(valid1),
			   .reset		(reset));
endmodule
