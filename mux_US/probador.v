module probador( // M�dulo probador: generador de se�ales y monitor de datos
	input		[31:0] data_out,
	input		[31:0] data_outE,
	output reg	[31:0] lineIN0,
	output reg	[31:0] lineIN1,
	output reg	valid0,
	output reg	valid1,
	output reg	reset,
	output reg	clk2S,
	output reg	clk);


	initial begin
		$dumpfile("Curvas.vcd");	// Nombre de archivo del "dump"
		$dumpvars;			// Directiva para "dumpear" variables
		// Mensaje que se imprime en consola una vez
		$display ("\t\t\tclk,\tlineIN0,\tlineIN1,\tdata_out");
		// Mensaje que se imprime en consola cada vez que un elemento de la lista cambia
		$monitor($time,"\t%b\t%b\t\t%b\t\t%b", clk, lineIN0, lineIN1, data_out);
		lineIN0 = 32'hDCBADCBA; 	// A los 3 bits, asigna un tipo bit con valor 0.
		lineIN1 = 32'hABCDABCD; 
		reset = 0;

						
		//for (i=0; i<5; i=i+1)begin
		repeat (8) begin		// Repite 8 veces
								// Sincronizaa con @(posedge clk); y luego,
				// hacer una asignaci�n no bloqueante ( <= )		
			@(posedge clk2S);				// Espera/sincroniza con el flanco positivo del reloj
			lineIN0 <= lineIN0 + 1;
			repeat (1) @(negedge clk2S);		// Repite 8 veces
			lineIN1 <= lineIN1 + 1;
	
		end
		@(posedge clk2S);	
		{lineIN0, lineIN0} <= 32'h00000000;	// Asigna un tipo bit con valor 0, de tama�o N

		$finish;			// Termina de almacenar se�ales
		//end
end
	// Reloj
	initial	begin
		clk 		<= 0;			// Valor inicial al reloj, sino siempre ser�
		clk2S 		<= 0;
		valid0 		<= 0;
		valid1 		<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	end
	always begin
		#2 clk 		<= ~clk;		// Hace "toggle" cada 2*10ns
					// Valor inicial al reloj, sino siempre
	end
	always begin
		#4 clk2S	<= ~clk2S;
	end

	always @(posedge clk) begin;				// Espera/sincroniza con el flanco positivo del reloj
		repeat(5) @(posedge clk);				
			valid1 	<= 1;
	end
	always @(posedge clk) begin;				// Espera/sincroniza con el flanco positivo del reloj
		repeat(3) @(posedge clk);				
			valid0 	<= 1;	
	end
	always @(posedge clk)begin
		repeat(3) @(posedge clk);	
			reset=1;						
	end
endmodule

