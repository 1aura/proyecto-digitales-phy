`timescale 	1ns				/ 100ps
// escala	unidad temporal (valor de "#1") / precisi�n
// includes de archivos de verilog
// Pueden omitirse y llamarse desde el testbench
`include "mux_US_C.v"
`include "mux_US_E.v"
`include "cmos_cells.v"
`include "probador.v"

module BancoPruebas; // Testbench
	// Por lo general, las se�ales en el banco de pruebas son wires.
	// No almacenan un valor, son manejadas por otras instancias de m�dulos.
	wire [31:0] data_out, data_outE, lineIN0, lineIN1;
	wire clk;

	// Descripci�n conductual de alarma
	mux_US_C		mux1C(/*AUTOINST*/
				      // Outputs
				      .data_out		(data_out[31:0]),
				      // Inputs
				      .lineIN0		(lineIN0[31:0]),
				      .lineIN1		(lineIN1[31:0]),
				      .valid0		(valid0),
				      .valid1		(valid1),
				      .clk		(clk),
				      .reset		(reset));

	mux_US_E		mux1E(/*AUTOINST*/
				      // Outputs
				      .data_out		(data_outE[31:0]),
				      // Inputs
				      .clk		(clk),
				      .lineIN0		(lineIN0[31:0]),
				      .lineIN1		(lineIN1[31:0]),
				      .reset		(reset),
				      .valid0		(valid0),
				      .valid1		(valid1));

	// Probador: generador de se�ales y monitor
	probador probador_(/*AUTOINST*/
			   // Outputs
			   .lineIN0		(lineIN0[31:0]),
			   .lineIN1		(lineIN1[31:0]),
			   .valid0		(valid0),
			   .valid1		(valid1),
			   .reset		(reset),
			   .clk2S		(clk2S),
			   .clk			(clk),
			   // Inputs
			   .data_out		(data_out[31:0]),
			   .data_outE		(data_outE[31:0]));
endmodule
