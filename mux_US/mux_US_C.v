module mux_US_C (
	input 		[31:0] lineIN0,	// Wire implicito. Entrada primaria
	input 		[31:0] lineIN1,	// Wire implicito. Entrada secundaria
	input		valid0,
	input		valid1,	
	input 		clk,		// Reloj para coordinar los cambios de las salidas
	input		reset,		
	output reg	[31:0] data_out); 

	reg selector;

	always @ (posedge clk) begin
		if (reset==0) begin
			data_out <= 32'h00000000;
			selector <=0;
		end	
		else begin			
			if (selector == 0)
				if (valid0 == 1)
					data_out <= lineIN0;
			if (selector == 1)
				if (valid1 == 1)
					data_out <= lineIN1;
			selector <= ~selector;

		end
	end
endmodule
