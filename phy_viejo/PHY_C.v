`include "phy_rx_C.v"
`include "phy_tx_C.v"

module  PHY_C(		// comportamiento del modulo phy completo
	input			clk_1MHz,
	input  			clk_2MHz,
	input  			clk_4MHz,
	input  			clk_32MHz,
	input [31:0] 	Entrada,
	input 			reset_x,
	input 			valido,
	output 			valid_out,
	output [31:0]	Salida);

wire serial1, serial2, activo0, activo1;

// Tx realizado con autoinst
phy_tx_C Tx (/*AUTOINST*/
	     // Outputs
	     .serial1			(serial1),
	     .serial2			(serial2),
	     // Inputs
	     .clk_2MHz			(clk_2MHz),
	     .clk_4MHz			(clk_4MHz),
	     .clk_32MHz			(clk_32MHz),
	     .Entrada			(Entrada[31:0]),
	     .reset_x			(reset_x),
	     .valido			(valido),
	     .activo0			(activo0),
	     .activo1			(activo1));

// Rx realizado con autoinst
phy_rx_C Rx (/*AUTOINST*/
	     // Outputs
	     .activo0			(activo0),
	     .activo1			(activo1),
	     .validout			(validout),
	     .Salida			(Salida[31:0]),
	     // Inputs
	     .clk_1MHz			(clk_1MHz),
	     .clk_2MHz			(clk_2MHz),
	     .clk_4MHz			(clk_4MHz),
	     .clk_32MHz			(clk_32MHz),
	     .reset_x			(reset_x),
	     .serial1			(serial1),
	     .serial2			(serial2));

	


endmodule
