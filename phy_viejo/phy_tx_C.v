`include "Union2.v"
`include "paraleloSerial_C.v"

module phy_tx_C(
input  clk_2MHz,
input  clk_4MHz,
input  clk_32MHz,
input [31:0] Entrada,
input reset_x,
input valido,
input activo0,
input activo1,
output		serial1,
output		serial2);

wire [7:0] A,B;
wire C,D, valid_salida1, valid_salida2;

Union2 entradar(.clk_2MHz(clk_2MHz),.clk_4MHz(clk_4MHz),.Entrada32(Entrada [31:0]),.reset_x(reset_x),.valido(valido),.activo0(activo0),.activo1(activo1),
.valido_out_0(C),.valido_out_1(D),.salidaDemux1(A[7:0]),.salidaDemux2(B[7:0]));

paraleloSerial_C PARALELO1(.clk_32MHz(clk_32MHz),.validIn(C),.reset(reset_x),.parallelInput(A[7:0]),
.serialOutput_C(serial1));

paraleloSerial_C PARALELO2(.clk_32MHz(clk_32MHz),.validIn(D),.reset(reset_x),.parallelInput(B[7:0]),
.serialOutput_C(serial2));



endmodule
