module Serial_Paralelo_C(	
	input 			clk_32MHz,
	input			clk_4MHz,
	input 			reset,           
	input 			serial_data,
	output reg		valid_out,
	output reg 		[7:0] serial_ParaleloOutput_C,
	output 			active);

	reg [7:0] 		BC;
	reg [7:0] 		tempy;
	reg [7:0] 		selector; 
	reg			active;
	reg [7:0]		contador_primero;
	reg atraso_valid;

always @(posedge clk_32MHz) begin
    if (reset == 0) begin
	serial_ParaleloOutput_C <= 8'h00;
	atraso_valid<=0;
	valid_out<=0;
	active			<= 0;
	BC			<= 0;
	tempy			<= 0;
	selector		<= 0;
	contador_primero	<= 0;
    end

    else if (reset == 1) begin
        
        if(active == 0) begin
		 atraso_valid<=0;
		//Una vez se libera el reset, se dania todo porque el selector empieza a correr.
		if (BC == 0)begin //Estado virgen, esta a la espera del primer BC. Para saber si recibe algo
				  //Partimos de la suposicion de que los primeros dos 00 menos significativos ya estan
			if (serial_data == 1)begin
				contador_primero <= contador_primero+1;
			end
			if (contador_primero == 4)begin  //Optimizar estas 6 lineas del temp
				tempy[7:0] <= 00111100;
	
				selector <= 7;
				BC	 <= BC + 1;
				contador_primero <= contador_primero+1;
			
			end
		end

		else begin
			
			case(selector)  
				    3'b000: tempy[0] <= serial_data;        
				    3'b001: tempy[1] <= serial_data;                
				    3'b010: tempy[2] <= serial_data;                
				    3'b011: tempy[3] <= serial_data;  
				    3'b100: tempy[4] <= serial_data;        
				    3'b101: tempy[5] <= serial_data;                
				    3'b110: tempy[6] <= serial_data;                
				    3'b111: tempy[7] <= serial_data;
			endcase 
			selector <= selector + 1;
			
			if (selector == 7)begin
				selector <= 0;
				if (tempy == 8'hBC)
					BC = BC+1;	
			end

			if (BC == 4)begin
				active 	<= 1;
				 atraso_valid<=1;
				
				BC 	<= 0;
				//serial_ParaleloOutput_C <= 8'b0;//Esto se borra (En todo caso los FF son temporales, justo en el clk que pasa, se actualiza con serial_data)
			end
		end

	end//End for active==0

	if(active == 1) begin
	 atraso_valid<= 1;
		case(selector) 
			    3'b000: tempy[0] <= serial_data;        
			    3'b001: tempy[1] <= serial_data;                
			    3'b010: tempy[2] <= serial_data;                
			    3'b011: tempy[3] <= serial_data;  
			    3'b100: tempy[4] <= serial_data;        
			    3'b101: tempy[5] <= serial_data;                
			    3'b110: tempy[6] <= serial_data;                
			    3'b111: tempy[7] <= serial_data;
		endcase 
		selector <= selector + 1;
		
		if (selector == 7)begin
			selector 		<= 0;
			serial_ParaleloOutput_C <= tempy;

				if (atraso_valid==1) begin
			
			valid_out<=1;
		end
		else begin
			
		
		valid_out<=0;
		end

		
		end
	
	end

        
    end//elseif
end
endmodule
