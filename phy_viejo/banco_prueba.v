`timescale 	1ns				/ 100ps
// escala	unidad temporal (valor de "#1") / precisi�n
// includes de archivos de verilog
`include "Probador.v"
`include "cmos_cells.v"
`include "PHY_C.v"
`include "PHY_E.v"


module BancoPruebas; // Testbench
	// Declarar todo como wires
	wire clk_1MHz, clk_2MHz, clk_4MHz, clk_32MHz, valido, valid_out;
	wire[31:0] Entrada, Salida;
	
	// phy_E: m�dulo estructural del phy
	PHY_E		PHY_E(/*AUTOINST*/
			      // Outputs
			      .Salida		(Salida[31:0]),
			      .valid_out	(valid_out),
			      // Inputs
			      .Entrada		(Entrada[31:0]),
			      .clk_1MHz		(clk_1MHz),
			      .clk_2MHz		(clk_2MHz),
			      .clk_32MHz	(clk_32MHz),
			      .clk_4MHz		(clk_4MHz),
			      .reset_x		(reset_x),
			      .valido		(valido));
	
	// phy_C: m�dulo estructural del phy
	PHY_C		PHY_C(/*AUTOINST*/
			      // Outputs
			      .valid_out	(valid_out),
			      .Salida		(Salida[31:0]),
			      // Inputs
			      .clk_1MHz		(clk_1MHz),
			      .clk_2MHz		(clk_2MHz),
			      .clk_4MHz		(clk_4MHz),
			      .clk_32MHz	(clk_32MHz),
			      .Entrada		(Entrada[31:0]),
			      .reset_x		(reset_x),
			      .valido		(valido));

	
	// probador: describir el probador
	Probador		Probador(/*AUTOINST*/
					 // Outputs
					 .clk_2MHz		(clk_2MHz),
					 .clk_4MHz		(clk_4MHz),
					 .clk_1MHz		(clk_1MHz),
					 .clk_32MHz		(clk_32MHz),
					 .Entrada		(Entrada[31:0]),
					 .reset_x		(reset_x),
					 .valido		(valido));
	
endmodule
