`include "recirculador.v"
`include "dmux2.v"
module Union1(
input  clk_2MHz,
input [31:0] Entrada,
input reset_x,
input valido,
input activo0,
input activo1,

output  [31:0] linea_0,
output  [31:0] linea_1,
output  valido_out0,
output  valido_out1 );

wire  [31:0] A,B;
recirculador recirculador_1(.clk (clk_2MHz),.datos(Entrada [31:0]),.active0(activo0),.active1(activo1),
.valid_entrada(valido),.reset(reset_x),.lane_active(A),.lane_probador(B),.valid_salida0(C),.valid_salida1(D));



dmux2 demux_1(.clk_2MHz (clk_2MHz),.data_in(A),.valid_int(C),.reset_L(reset_x),.lane_0(linea_0 [31:0]),
.lane_1(linea_1 [31:0]),.valid_out0(valido_out0) ,.valid_out1(valido_out1));



endmodule
