`include "Union1.v"
`include "demux_32x8_C.v"
module Union2(
input  clk_2MHz,
input  clk_4MHz,
input [31:0] Entrada32,
input reset_x,
input valido,
input activo0,
input activo1,

output 			valido_out_1,
output    valido_out_0,
output [7:0] 	salidaDemux1,
output  [7:0] 	salidaDemux2
);

wire [31:0] A,B;
wire C,D;


Union1 primeraxx(.clk_2MHz (clk_2MHz),.Entrada (Entrada32),.activo0 (activo0), .activo1(activo1),
.valido(valido),.reset_x(reset_x),.linea_0(A [31:0]),.linea_1(B [31:0]),.valido_out0(C),.valido_out1(D) )
;
demux_32x8_C demux1(.clk_4MHz(clk_4MHz),.reset(reset_x),
.demuxInput(A [31:0]),.validIn(C),.validOut(valido_out_0),.salidaDemux_C(salidaDemux1));

demux_32x8_C demux2(.clk_4MHz(clk_4MHz),.reset(reset_x),
.demuxInput(B [31:0]),.validIn(D),.validOut(valido_out_1),.salidaDemux_C(salidaDemux2)); 
endmodule




