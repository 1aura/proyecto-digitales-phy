module mux_US_C (
	input 		[31:0] lineIN0,	// Wire implicito. Entrada primaria
	input 		[31:0] lineIN1,	// Wire implicito. Entrada secundaria
	input		valid0,
	input		valid1,	
	input 		clk,		// Reloj para coordinar los cambios de las salidas
	input		reset,
	output reg	validout,		
	output reg	[31:0] data_out); 

	reg selector;

	always @ (posedge clk) begin
	
		if (reset==0) begin
			data_out <= 32'h00000000;
			selector <=0;
			validout <=0;
		end	
		else begin			
			if (selector == 0) begin
				if (valid0 == 1) begin
					data_out <= lineIN0;
					validout <= 1;	
				end
				else begin
					data_out <= 32'h00000000;
					validout <= 0;	
				end
			end
			else if (selector == 1) begin
				if (valid1 == 1) begin
					data_out <= lineIN1;
					validout <= 1;	
				end
				else begin
					data_out <= 32'h00000000;
					validout <= 0;	
				end
			end
			selector <= ~selector;
		end
		
	end
endmodule
