module dmux2(
input  clk_2MHz,
input [31:0]data_in,
input valid_int,
input reset_L,
output reg [31:0] lane_0,
output reg [31:0] lane_1,
output reg valid_out0,
output reg valid_out1

);
reg sel;
always@(posedge clk_2MHz) begin 

 if(reset_L==0)  begin  
  lane_1<=31'b0;
  lane_0<=31'b0;
  valid_out0<=31'b0;
  valid_out1<=31'b0;
  sel<=0;
  
end

else if (valid_int==0) begin
lane_1<=31'b0;
  lane_0<=31'b0;
  valid_out0<=31'b0;
  valid_out1<=31'b0;

  end


else  begin
  
  if (sel==0) begin 
    if (valid_int==1)begin
    lane_0<=data_in;
    valid_out0<=valid_int;
   end
   end
  if  (sel==1)  begin
    
 
    if (valid_int==1)begin
    lane_1<=data_in;
    valid_out1<=valid_int;
 end
 end
 sel <= ~ sel;
  
  end
   
  
end 
endmodule
