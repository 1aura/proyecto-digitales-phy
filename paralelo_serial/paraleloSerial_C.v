module paraleloSerial_C(	// comportamiento del demux 32x8
	input 				clk_32MHz,
	input				validIn,
	input 				reset,
	input [7:0] 		parallelInput,
	output reg 			serialOutput_C);
					

reg[2:0] selector; 

//Revisar entradas en cada flanco positivo del reloj
always @(posedge clk_32MHz) begin // DEMUX code

	if (reset == 0) begin
		serialOutput_C <= 'b0;
		selector <= 0;
	end
	else if (validIn == 0) begin
		selector <= selector + 1;
		case(selector)
			3'b000: serialOutput_C <= 0;        
			3'b001: serialOutput_C <= 0;                
			3'b010: serialOutput_C <= 1;                
			3'b011: serialOutput_C <= 1;  
			3'b100: serialOutput_C <= 1;        
			3'b101: serialOutput_C <= 1;                
			3'b110: serialOutput_C <= 0;                
			3'b111: serialOutput_C <= 1;                       
		endcase   
	end
	else begin	
		selector <= selector + 1;
		case(selector)
			3'b000: serialOutput_C <= parallelInput[7];        
			3'b001: serialOutput_C <= parallelInput[6];                
			3'b010: serialOutput_C <= parallelInput[5];                
			3'b011: serialOutput_C <= parallelInput[4];  
			3'b100: serialOutput_C <= parallelInput[3];        
			3'b101: serialOutput_C <= parallelInput[2];                
			3'b110: serialOutput_C <= parallelInput[1];                
			3'b111: serialOutput_C <= parallelInput[0];                       
		endcase   
	end
	
end

endmodule
