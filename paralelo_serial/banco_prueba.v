`timescale 	1ns				/ 100ps
// escala	unidad temporal (valor de "#1") / precisión
// includes de archivos de verilog
`include "probadorParaleloSerial.v"
`include "paraleloSerial_E.v"
`include "paraleloSerial_C.v"
`include "cmos_cells.v"


module BancoPruebas; // Testbench
	// Declarar todo como wires
	wire clk_1MHz, clk_2MHz, clk_4MHz, clk_32MHz, validIn, reset;
	wire salidaDemux_C, salidaDemux_E;
	wire[7:0] parallelInput;
	
	// demux conductual
	paraleloSerial_C	paraleloSerial_C(/*AUTOINST*/
						 // Outputs
						 .serialOutput_C	(serialOutput_C),
						 // Inputs
						 .clk_32MHz		(clk_32MHz),
						 .validIn		(validIn),
						 .reset			(reset),
						 .parallelInput		(parallelInput[7:0]));
					     
	// demux estructural
	paraleloSerial_E	paraleloSerial_E(/*AUTOINST*/
						 // Outputs
						 .serialOutput_E	(serialOutput_E),
						 // Inputs
						 .clk_32MHz		(clk_32MHz),
						 .parallelInput		(parallelInput[7:0]),
						 .reset			(reset),
						 .validIn		(validIn));
	
	// probador: describir el probador
	probadorParaleloSerial		probadorParaleloSerial(/*AUTOINST*/
							       // Outputs
							       .clk_1MHz	(clk_1MHz),
							       .clk_2MHz	(clk_2MHz),
							       .clk_4MHz	(clk_4MHz),
							       .clk_32MHz	(clk_32MHz),
							       .validIn		(validIn),
							       .reset		(reset),
							       .parallelInput	(parallelInput[7:0]),
							       // Inputs
							       .serialOutput_C	(serialOutput_C),
							       .serialOutput_E	(serialOutput_E));
	
endmodule
