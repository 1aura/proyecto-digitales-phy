module mux_8x32_C(	// comportamiento del multiplexor interno
	input				clk_4MHz,
	input				validIn,
	input				reset,
	input [7:0] 		muxInput,
	output reg [31:0]	salidaMux_C,
	output reg			validOut);

reg			endOfWord, internalValid, delayedValid1, delayedValid2, delayedValid3;
reg[1:0]	selector;
reg[31:0]	temp, salida;

// Revisar entradas en cada flanco positivo del reloj rápido
always @(posedge clk_4MHz) begin // DEMUX code

	// actualizar otros valids
	delayedValid1 <= internalValid;
	delayedValid2 <= delayedValid1;
	delayedValid3 <= delayedValid2;

	// caso de reset abajo
	if(reset == 0) begin
		selector <= 'b00;
		temp <= 'b00;
		endOfWord <= 0;
		internalValid <= 0;
	end
	// caso señal no valid abajo
	else if (validIn == 0) begin
		selector <= 'b00;
		temp <= 'b00;
		endOfWord <= 0;
		internalValid <= 0;
	end
	// señal válida y reset = 1
	else begin
		// incrementar selector
		selector <= selector + 1;
		
		// revisar input
		case(selector)
			2'b11: temp[7:0] <= muxInput;        
			2'b10: temp[15:8] <= muxInput;                
			2'b01: temp[23:16] <= muxInput;                
			2'b00: temp[31:24] <= muxInput;                       
		endcase 
		
		// si selector == 'b11, registrar fin de palabra
		if(selector == 'b11) begin
			endOfWord <= 1;
			internalValid <= 0;
		end
		// revisar si es el final de la palabra, seleccionar valor de internalValid
		else if(endOfWord == 1) begin
			salida <= temp;
			internalValid <= 1;
			endOfWord <= 0;
		end
		// cualquier otro caso
		else begin
			internalValid <= 0;
		end
		
	end
	
	
	// SIEMPRE actualizar salida
	if ((internalValid == 1)||(delayedValid1 == 1)||(delayedValid2 == 1)||(delayedValid3 == 1)) begin
		salidaMux_C <= salida;
		validOut <= 'b1;
	end
	else begin
		salidaMux_C <= 'b0;
		validOut <= 'b0;
	end
	
end




endmodule
