/* Generated by Yosys 0.9 (git sha1 1979e0b) */

(* top =  1  *)
(* src = "mux_8x32_C.v:1" *)
module mux_8x32_E(clk_4MHz, validIn, reset, muxInput, salidaMux_E, validOut);
  (* src = "mux_8x32_C.v:14" *)
  wire _000_;
  (* src = "mux_8x32_C.v:14" *)
  wire _001_;
  (* src = "mux_8x32_C.v:14" *)
  wire [31:0] _002_;
  (* src = "mux_8x32_C.v:14" *)
  wire [31:0] _003_;
  (* src = "mux_8x32_C.v:14" *)
  wire [1:0] _004_;
  (* src = "mux_8x32_C.v:14" *)
  wire [31:0] _005_;
  (* src = "mux_8x32_C.v:14" *)
  wire _006_;
  wire _007_;
  wire _008_;
  wire _009_;
  wire _010_;
  wire _011_;
  wire _012_;
  wire _013_;
  wire _014_;
  wire _015_;
  wire _016_;
  wire _017_;
  wire _018_;
  wire _019_;
  wire _020_;
  wire _021_;
  wire _022_;
  wire _023_;
  wire _024_;
  wire _025_;
  wire _026_;
  wire _027_;
  wire _028_;
  wire _029_;
  wire _030_;
  wire _031_;
  wire _032_;
  wire _033_;
  wire _034_;
  wire _035_;
  wire _036_;
  wire _037_;
  wire _038_;
  wire _039_;
  wire _040_;
  wire _041_;
  wire _042_;
  wire _043_;
  wire _044_;
  wire _045_;
  wire _046_;
  wire _047_;
  wire _048_;
  wire _049_;
  wire _050_;
  wire _051_;
  wire _052_;
  wire _053_;
  wire _054_;
  wire _055_;
  wire _056_;
  wire _057_;
  wire _058_;
  wire _059_;
  wire _060_;
  wire _061_;
  wire _062_;
  wire _063_;
  wire _064_;
  wire _065_;
  wire _066_;
  wire _067_;
  wire _068_;
  wire _069_;
  wire _070_;
  wire _071_;
  wire _072_;
  wire _073_;
  wire _074_;
  wire _075_;
  wire _076_;
  wire _077_;
  wire _078_;
  wire _079_;
  wire _080_;
  wire _081_;
  wire _082_;
  wire _083_;
  wire _084_;
  wire _085_;
  wire _086_;
  wire _087_;
  wire _088_;
  wire _089_;
  wire _090_;
  wire _091_;
  wire _092_;
  wire _093_;
  wire _094_;
  wire _095_;
  wire _096_;
  wire _097_;
  wire _098_;
  wire _099_;
  wire _100_;
  wire _101_;
  wire _102_;
  wire _103_;
  wire _104_;
  wire _105_;
  wire _106_;
  wire _107_;
  wire _108_;
  wire _109_;
  wire _110_;
  wire _111_;
  wire _112_;
  wire _113_;
  wire _114_;
  wire _115_;
  wire _116_;
  wire _117_;
  wire _118_;
  wire _119_;
  wire _120_;
  wire _121_;
  wire _122_;
  wire _123_;
  wire _124_;
  wire _125_;
  wire _126_;
  wire _127_;
  wire _128_;
  wire _129_;
  wire _130_;
  wire _131_;
  wire _132_;
  wire _133_;
  wire _134_;
  wire _135_;
  wire _136_;
  wire _137_;
  wire _138_;
  wire _139_;
  wire _140_;
  wire _141_;
  wire _142_;
  wire _143_;
  wire _144_;
  wire _145_;
  wire _146_;
  wire _147_;
  wire _148_;
  wire _149_;
  wire _150_;
  wire _151_;
  wire _152_;
  wire _153_;
  wire _154_;
  wire _155_;
  wire _156_;
  wire _157_;
  wire _158_;
  wire _159_;
  wire _160_;
  wire _161_;
  wire _162_;
  wire _163_;
  wire _164_;
  wire _165_;
  wire _166_;
  wire _167_;
  wire _168_;
  wire _169_;
  wire _170_;
  wire _171_;
  wire _172_;
  wire _173_;
  wire _174_;
  wire _175_;
  wire _176_;
  wire _177_;
  wire _178_;
  wire _179_;
  wire _180_;
  wire _181_;
  wire _182_;
  wire _183_;
  wire _184_;
  wire _185_;
  wire _186_;
  wire _187_;
  wire _188_;
  wire _189_;
  wire _190_;
  wire _191_;
  wire _192_;
  wire _193_;
  wire _194_;
  wire _195_;
  wire _196_;
  wire _197_;
  wire _198_;
  wire _199_;
  wire _200_;
  wire _201_;
  wire _202_;
  wire _203_;
  wire _204_;
  wire _205_;
  wire _206_;
  wire _207_;
  wire _208_;
  wire _209_;
  wire _210_;
  wire _211_;
  wire _212_;
  wire _213_;
  wire _214_;
  wire _215_;
  wire _216_;
  wire _217_;
  (* src = "mux_8x32_C.v:2" *)
  input clk_4MHz;
  (* src = "mux_8x32_C.v:9" *)
  wire delayedValid1;
  (* src = "mux_8x32_C.v:9" *)
  wire delayedValid2;
  (* src = "mux_8x32_C.v:9" *)
  wire delayedValid3;
  (* src = "mux_8x32_C.v:9" *)
  wire endOfWord;
  (* src = "mux_8x32_C.v:9" *)
  wire internalValid;
  (* src = "mux_8x32_C.v:5" *)
  input [7:0] muxInput;
  (* src = "mux_8x32_C.v:4" *)
  input reset;
  (* src = "mux_8x32_C.v:11" *)
  wire [31:0] salida;
  (* src = "mux_8x32_C.v:6" *)
  output [31:0] salidaMux_E;
  (* src = "mux_8x32_C.v:10" *)
  wire [1:0] selector;
  (* src = "mux_8x32_C.v:11" *)
  wire [31:0] temp;
  (* src = "mux_8x32_C.v:3" *)
  input validIn;
  (* src = "mux_8x32_C.v:7" *)
  output validOut;
  NOT _218_ (
    .A(selector[0]),
    .Y(_007_)
  );
  NOT _219_ (
    .A(selector[1]),
    .Y(_008_)
  );
  NOT _220_ (
    .A(muxInput[0]),
    .Y(_009_)
  );
  NOT _221_ (
    .A(muxInput[1]),
    .Y(_010_)
  );
  NOT _222_ (
    .A(muxInput[2]),
    .Y(_011_)
  );
  NOT _223_ (
    .A(muxInput[3]),
    .Y(_012_)
  );
  NOT _224_ (
    .A(muxInput[4]),
    .Y(_013_)
  );
  NOT _225_ (
    .A(muxInput[5]),
    .Y(_014_)
  );
  NOT _226_ (
    .A(muxInput[6]),
    .Y(_015_)
  );
  NOT _227_ (
    .A(muxInput[7]),
    .Y(_016_)
  );
  NOT _228_ (
    .A(validIn),
    .Y(_017_)
  );
  NOT _229_ (
    .A(reset),
    .Y(_018_)
  );
  NOT _230_ (
    .A(salida[0]),
    .Y(_019_)
  );
  NOT _231_ (
    .A(salida[1]),
    .Y(_020_)
  );
  NOT _232_ (
    .A(salida[2]),
    .Y(_021_)
  );
  NOT _233_ (
    .A(salida[3]),
    .Y(_022_)
  );
  NOT _234_ (
    .A(salida[4]),
    .Y(_023_)
  );
  NOT _235_ (
    .A(salida[5]),
    .Y(_024_)
  );
  NOT _236_ (
    .A(salida[6]),
    .Y(_025_)
  );
  NOT _237_ (
    .A(salida[7]),
    .Y(_026_)
  );
  NOT _238_ (
    .A(salida[8]),
    .Y(_027_)
  );
  NOT _239_ (
    .A(salida[9]),
    .Y(_028_)
  );
  NOT _240_ (
    .A(salida[10]),
    .Y(_029_)
  );
  NOT _241_ (
    .A(salida[11]),
    .Y(_030_)
  );
  NOT _242_ (
    .A(salida[12]),
    .Y(_031_)
  );
  NOT _243_ (
    .A(salida[13]),
    .Y(_032_)
  );
  NOT _244_ (
    .A(salida[14]),
    .Y(_033_)
  );
  NOT _245_ (
    .A(salida[15]),
    .Y(_034_)
  );
  NOT _246_ (
    .A(salida[16]),
    .Y(_035_)
  );
  NOT _247_ (
    .A(salida[17]),
    .Y(_036_)
  );
  NOT _248_ (
    .A(salida[18]),
    .Y(_037_)
  );
  NOT _249_ (
    .A(salida[19]),
    .Y(_038_)
  );
  NOT _250_ (
    .A(salida[20]),
    .Y(_039_)
  );
  NOT _251_ (
    .A(salida[21]),
    .Y(_040_)
  );
  NOT _252_ (
    .A(salida[22]),
    .Y(_041_)
  );
  NOT _253_ (
    .A(salida[23]),
    .Y(_042_)
  );
  NOT _254_ (
    .A(salida[24]),
    .Y(_043_)
  );
  NOT _255_ (
    .A(salida[25]),
    .Y(_044_)
  );
  NOT _256_ (
    .A(salida[26]),
    .Y(_045_)
  );
  NOT _257_ (
    .A(salida[27]),
    .Y(_046_)
  );
  NOT _258_ (
    .A(salida[28]),
    .Y(_047_)
  );
  NOT _259_ (
    .A(salida[29]),
    .Y(_048_)
  );
  NOT _260_ (
    .A(salida[30]),
    .Y(_049_)
  );
  NOT _261_ (
    .A(salida[31]),
    .Y(_050_)
  );
  NOR _262_ (
    .A(delayedValid2),
    .B(internalValid),
    .Y(_051_)
  );
  NOT _263_ (
    .A(_051_),
    .Y(_052_)
  );
  NOR _264_ (
    .A(delayedValid1),
    .B(delayedValid3),
    .Y(_053_)
  );
  NOT _265_ (
    .A(_053_),
    .Y(_054_)
  );
  NOR _266_ (
    .A(_052_),
    .B(_054_),
    .Y(_055_)
  );
  NOT _267_ (
    .A(_055_),
    .Y(_006_)
  );
  NOR _268_ (
    .A(_017_),
    .B(_018_),
    .Y(_056_)
  );
  NAND _269_ (
    .A(validIn),
    .B(reset),
    .Y(_057_)
  );
  NOR _270_ (
    .A(selector[0]),
    .B(selector[1]),
    .Y(_058_)
  );
  NOR _271_ (
    .A(temp[24]),
    .B(_058_),
    .Y(_059_)
  );
  NAND _272_ (
    .A(_009_),
    .B(_058_),
    .Y(_060_)
  );
  NAND _273_ (
    .A(_056_),
    .B(_060_),
    .Y(_061_)
  );
  NOR _274_ (
    .A(_059_),
    .B(_061_),
    .Y(_005_[24])
  );
  NOR _275_ (
    .A(temp[25]),
    .B(_058_),
    .Y(_062_)
  );
  NAND _276_ (
    .A(_010_),
    .B(_058_),
    .Y(_063_)
  );
  NAND _277_ (
    .A(_056_),
    .B(_063_),
    .Y(_064_)
  );
  NOR _278_ (
    .A(_062_),
    .B(_064_),
    .Y(_005_[25])
  );
  NOR _279_ (
    .A(temp[26]),
    .B(_058_),
    .Y(_065_)
  );
  NAND _280_ (
    .A(_011_),
    .B(_058_),
    .Y(_066_)
  );
  NAND _281_ (
    .A(_056_),
    .B(_066_),
    .Y(_067_)
  );
  NOR _282_ (
    .A(_065_),
    .B(_067_),
    .Y(_005_[26])
  );
  NOR _283_ (
    .A(temp[27]),
    .B(_058_),
    .Y(_068_)
  );
  NAND _284_ (
    .A(_012_),
    .B(_058_),
    .Y(_069_)
  );
  NAND _285_ (
    .A(_056_),
    .B(_069_),
    .Y(_070_)
  );
  NOR _286_ (
    .A(_068_),
    .B(_070_),
    .Y(_005_[27])
  );
  NOR _287_ (
    .A(temp[28]),
    .B(_058_),
    .Y(_071_)
  );
  NAND _288_ (
    .A(_013_),
    .B(_058_),
    .Y(_072_)
  );
  NAND _289_ (
    .A(_056_),
    .B(_072_),
    .Y(_073_)
  );
  NOR _290_ (
    .A(_071_),
    .B(_073_),
    .Y(_005_[28])
  );
  NOR _291_ (
    .A(temp[29]),
    .B(_058_),
    .Y(_074_)
  );
  NAND _292_ (
    .A(_014_),
    .B(_058_),
    .Y(_075_)
  );
  NAND _293_ (
    .A(_056_),
    .B(_075_),
    .Y(_076_)
  );
  NOR _294_ (
    .A(_074_),
    .B(_076_),
    .Y(_005_[29])
  );
  NOR _295_ (
    .A(temp[30]),
    .B(_058_),
    .Y(_077_)
  );
  NAND _296_ (
    .A(_015_),
    .B(_058_),
    .Y(_078_)
  );
  NAND _297_ (
    .A(_056_),
    .B(_078_),
    .Y(_079_)
  );
  NOR _298_ (
    .A(_077_),
    .B(_079_),
    .Y(_005_[30])
  );
  NOR _299_ (
    .A(temp[31]),
    .B(_058_),
    .Y(_080_)
  );
  NAND _300_ (
    .A(_016_),
    .B(_058_),
    .Y(_081_)
  );
  NAND _301_ (
    .A(_056_),
    .B(_081_),
    .Y(_082_)
  );
  NOR _302_ (
    .A(_080_),
    .B(_082_),
    .Y(_005_[31])
  );
  NOR _303_ (
    .A(_007_),
    .B(selector[1]),
    .Y(_083_)
  );
  NOR _304_ (
    .A(temp[16]),
    .B(_083_),
    .Y(_084_)
  );
  NAND _305_ (
    .A(_009_),
    .B(_083_),
    .Y(_085_)
  );
  NAND _306_ (
    .A(_056_),
    .B(_085_),
    .Y(_086_)
  );
  NOR _307_ (
    .A(_084_),
    .B(_086_),
    .Y(_005_[16])
  );
  NOR _308_ (
    .A(temp[17]),
    .B(_083_),
    .Y(_087_)
  );
  NAND _309_ (
    .A(_010_),
    .B(_083_),
    .Y(_088_)
  );
  NAND _310_ (
    .A(_056_),
    .B(_088_),
    .Y(_089_)
  );
  NOR _311_ (
    .A(_087_),
    .B(_089_),
    .Y(_005_[17])
  );
  NOR _312_ (
    .A(temp[18]),
    .B(_083_),
    .Y(_090_)
  );
  NAND _313_ (
    .A(_011_),
    .B(_083_),
    .Y(_091_)
  );
  NAND _314_ (
    .A(_056_),
    .B(_091_),
    .Y(_092_)
  );
  NOR _315_ (
    .A(_090_),
    .B(_092_),
    .Y(_005_[18])
  );
  NOR _316_ (
    .A(temp[19]),
    .B(_083_),
    .Y(_093_)
  );
  NAND _317_ (
    .A(_012_),
    .B(_083_),
    .Y(_094_)
  );
  NAND _318_ (
    .A(_056_),
    .B(_094_),
    .Y(_095_)
  );
  NOR _319_ (
    .A(_093_),
    .B(_095_),
    .Y(_005_[19])
  );
  NOR _320_ (
    .A(temp[20]),
    .B(_083_),
    .Y(_096_)
  );
  NAND _321_ (
    .A(_013_),
    .B(_083_),
    .Y(_097_)
  );
  NAND _322_ (
    .A(_056_),
    .B(_097_),
    .Y(_098_)
  );
  NOR _323_ (
    .A(_096_),
    .B(_098_),
    .Y(_005_[20])
  );
  NOR _324_ (
    .A(temp[21]),
    .B(_083_),
    .Y(_099_)
  );
  NAND _325_ (
    .A(_014_),
    .B(_083_),
    .Y(_100_)
  );
  NAND _326_ (
    .A(_056_),
    .B(_100_),
    .Y(_101_)
  );
  NOR _327_ (
    .A(_099_),
    .B(_101_),
    .Y(_005_[21])
  );
  NOR _328_ (
    .A(temp[22]),
    .B(_083_),
    .Y(_102_)
  );
  NAND _329_ (
    .A(_015_),
    .B(_083_),
    .Y(_103_)
  );
  NAND _330_ (
    .A(_056_),
    .B(_103_),
    .Y(_104_)
  );
  NOR _331_ (
    .A(_102_),
    .B(_104_),
    .Y(_005_[22])
  );
  NOR _332_ (
    .A(temp[23]),
    .B(_083_),
    .Y(_105_)
  );
  NAND _333_ (
    .A(_016_),
    .B(_083_),
    .Y(_106_)
  );
  NAND _334_ (
    .A(_056_),
    .B(_106_),
    .Y(_107_)
  );
  NOR _335_ (
    .A(_105_),
    .B(_107_),
    .Y(_005_[23])
  );
  NOR _336_ (
    .A(selector[0]),
    .B(_008_),
    .Y(_108_)
  );
  NOR _337_ (
    .A(temp[8]),
    .B(_108_),
    .Y(_109_)
  );
  NAND _338_ (
    .A(_009_),
    .B(_108_),
    .Y(_110_)
  );
  NAND _339_ (
    .A(_056_),
    .B(_110_),
    .Y(_111_)
  );
  NOR _340_ (
    .A(_109_),
    .B(_111_),
    .Y(_005_[8])
  );
  NOR _341_ (
    .A(temp[9]),
    .B(_108_),
    .Y(_112_)
  );
  NAND _342_ (
    .A(_010_),
    .B(_108_),
    .Y(_113_)
  );
  NAND _343_ (
    .A(_056_),
    .B(_113_),
    .Y(_114_)
  );
  NOR _344_ (
    .A(_112_),
    .B(_114_),
    .Y(_005_[9])
  );
  NOR _345_ (
    .A(temp[10]),
    .B(_108_),
    .Y(_115_)
  );
  NAND _346_ (
    .A(_011_),
    .B(_108_),
    .Y(_116_)
  );
  NAND _347_ (
    .A(_056_),
    .B(_116_),
    .Y(_117_)
  );
  NOR _348_ (
    .A(_115_),
    .B(_117_),
    .Y(_005_[10])
  );
  NOR _349_ (
    .A(temp[11]),
    .B(_108_),
    .Y(_118_)
  );
  NAND _350_ (
    .A(_012_),
    .B(_108_),
    .Y(_119_)
  );
  NAND _351_ (
    .A(_056_),
    .B(_119_),
    .Y(_120_)
  );
  NOR _352_ (
    .A(_118_),
    .B(_120_),
    .Y(_005_[11])
  );
  NOR _353_ (
    .A(temp[12]),
    .B(_108_),
    .Y(_121_)
  );
  NAND _354_ (
    .A(_013_),
    .B(_108_),
    .Y(_122_)
  );
  NAND _355_ (
    .A(_056_),
    .B(_122_),
    .Y(_123_)
  );
  NOR _356_ (
    .A(_121_),
    .B(_123_),
    .Y(_005_[12])
  );
  NOR _357_ (
    .A(temp[13]),
    .B(_108_),
    .Y(_124_)
  );
  NAND _358_ (
    .A(_014_),
    .B(_108_),
    .Y(_125_)
  );
  NAND _359_ (
    .A(_056_),
    .B(_125_),
    .Y(_126_)
  );
  NOR _360_ (
    .A(_124_),
    .B(_126_),
    .Y(_005_[13])
  );
  NOR _361_ (
    .A(temp[14]),
    .B(_108_),
    .Y(_127_)
  );
  NAND _362_ (
    .A(_015_),
    .B(_108_),
    .Y(_128_)
  );
  NAND _363_ (
    .A(_056_),
    .B(_128_),
    .Y(_129_)
  );
  NOR _364_ (
    .A(_127_),
    .B(_129_),
    .Y(_005_[14])
  );
  NOR _365_ (
    .A(temp[15]),
    .B(_108_),
    .Y(_130_)
  );
  NAND _366_ (
    .A(_016_),
    .B(_108_),
    .Y(_131_)
  );
  NAND _367_ (
    .A(_056_),
    .B(_131_),
    .Y(_132_)
  );
  NOR _368_ (
    .A(_130_),
    .B(_132_),
    .Y(_005_[15])
  );
  NAND _369_ (
    .A(selector[0]),
    .B(selector[1]),
    .Y(_133_)
  );
  NOT _370_ (
    .A(_133_),
    .Y(_134_)
  );
  NOR _371_ (
    .A(_057_),
    .B(_134_),
    .Y(_135_)
  );
  NAND _372_ (
    .A(_056_),
    .B(_133_),
    .Y(_136_)
  );
  NAND _373_ (
    .A(temp[0]),
    .B(_135_),
    .Y(_137_)
  );
  NOR _374_ (
    .A(_057_),
    .B(_133_),
    .Y(_000_)
  );
  NAND _375_ (
    .A(muxInput[0]),
    .B(_000_),
    .Y(_138_)
  );
  NAND _376_ (
    .A(_137_),
    .B(_138_),
    .Y(_005_[0])
  );
  NAND _377_ (
    .A(temp[1]),
    .B(_135_),
    .Y(_139_)
  );
  NAND _378_ (
    .A(muxInput[1]),
    .B(_000_),
    .Y(_140_)
  );
  NAND _379_ (
    .A(_139_),
    .B(_140_),
    .Y(_005_[1])
  );
  NAND _380_ (
    .A(temp[2]),
    .B(_135_),
    .Y(_141_)
  );
  NAND _381_ (
    .A(muxInput[2]),
    .B(_000_),
    .Y(_142_)
  );
  NAND _382_ (
    .A(_141_),
    .B(_142_),
    .Y(_005_[2])
  );
  NAND _383_ (
    .A(temp[3]),
    .B(_135_),
    .Y(_143_)
  );
  NAND _384_ (
    .A(muxInput[3]),
    .B(_000_),
    .Y(_144_)
  );
  NAND _385_ (
    .A(_143_),
    .B(_144_),
    .Y(_005_[3])
  );
  NAND _386_ (
    .A(temp[4]),
    .B(_135_),
    .Y(_145_)
  );
  NAND _387_ (
    .A(muxInput[4]),
    .B(_000_),
    .Y(_146_)
  );
  NAND _388_ (
    .A(_145_),
    .B(_146_),
    .Y(_005_[4])
  );
  NAND _389_ (
    .A(temp[5]),
    .B(_135_),
    .Y(_147_)
  );
  NAND _390_ (
    .A(muxInput[5]),
    .B(_000_),
    .Y(_148_)
  );
  NAND _391_ (
    .A(_147_),
    .B(_148_),
    .Y(_005_[5])
  );
  NAND _392_ (
    .A(temp[6]),
    .B(_135_),
    .Y(_149_)
  );
  NAND _393_ (
    .A(muxInput[6]),
    .B(_000_),
    .Y(_150_)
  );
  NAND _394_ (
    .A(_149_),
    .B(_150_),
    .Y(_005_[6])
  );
  NAND _395_ (
    .A(temp[7]),
    .B(_135_),
    .Y(_151_)
  );
  NAND _396_ (
    .A(muxInput[7]),
    .B(_000_),
    .Y(_152_)
  );
  NAND _397_ (
    .A(_151_),
    .B(_152_),
    .Y(_005_[7])
  );
  NAND _398_ (
    .A(endOfWord),
    .B(_135_),
    .Y(_153_)
  );
  NOT _399_ (
    .A(_153_),
    .Y(_001_)
  );
  NOR _400_ (
    .A(temp[0]),
    .B(_153_),
    .Y(_154_)
  );
  NOR _401_ (
    .A(salida[0]),
    .B(_001_),
    .Y(_155_)
  );
  NOR _402_ (
    .A(_154_),
    .B(_155_),
    .Y(_003_[0])
  );
  NOR _403_ (
    .A(temp[1]),
    .B(_153_),
    .Y(_156_)
  );
  NOR _404_ (
    .A(salida[1]),
    .B(_001_),
    .Y(_157_)
  );
  NOR _405_ (
    .A(_156_),
    .B(_157_),
    .Y(_003_[1])
  );
  NOR _406_ (
    .A(temp[2]),
    .B(_153_),
    .Y(_158_)
  );
  NOR _407_ (
    .A(salida[2]),
    .B(_001_),
    .Y(_159_)
  );
  NOR _408_ (
    .A(_158_),
    .B(_159_),
    .Y(_003_[2])
  );
  NOR _409_ (
    .A(temp[3]),
    .B(_153_),
    .Y(_160_)
  );
  NOR _410_ (
    .A(salida[3]),
    .B(_001_),
    .Y(_161_)
  );
  NOR _411_ (
    .A(_160_),
    .B(_161_),
    .Y(_003_[3])
  );
  NOR _412_ (
    .A(temp[4]),
    .B(_153_),
    .Y(_162_)
  );
  NOR _413_ (
    .A(salida[4]),
    .B(_001_),
    .Y(_163_)
  );
  NOR _414_ (
    .A(_162_),
    .B(_163_),
    .Y(_003_[4])
  );
  NOR _415_ (
    .A(temp[5]),
    .B(_153_),
    .Y(_164_)
  );
  NOR _416_ (
    .A(salida[5]),
    .B(_001_),
    .Y(_165_)
  );
  NOR _417_ (
    .A(_164_),
    .B(_165_),
    .Y(_003_[5])
  );
  NOR _418_ (
    .A(temp[6]),
    .B(_153_),
    .Y(_166_)
  );
  NOR _419_ (
    .A(salida[6]),
    .B(_001_),
    .Y(_167_)
  );
  NOR _420_ (
    .A(_166_),
    .B(_167_),
    .Y(_003_[6])
  );
  NOR _421_ (
    .A(temp[7]),
    .B(_153_),
    .Y(_168_)
  );
  NOR _422_ (
    .A(salida[7]),
    .B(_001_),
    .Y(_169_)
  );
  NOR _423_ (
    .A(_168_),
    .B(_169_),
    .Y(_003_[7])
  );
  NAND _424_ (
    .A(temp[8]),
    .B(_001_),
    .Y(_170_)
  );
  NAND _425_ (
    .A(salida[8]),
    .B(_153_),
    .Y(_171_)
  );
  NAND _426_ (
    .A(_170_),
    .B(_171_),
    .Y(_003_[8])
  );
  NAND _427_ (
    .A(temp[9]),
    .B(_001_),
    .Y(_172_)
  );
  NAND _428_ (
    .A(salida[9]),
    .B(_153_),
    .Y(_173_)
  );
  NAND _429_ (
    .A(_172_),
    .B(_173_),
    .Y(_003_[9])
  );
  NAND _430_ (
    .A(temp[10]),
    .B(_001_),
    .Y(_174_)
  );
  NAND _431_ (
    .A(salida[10]),
    .B(_153_),
    .Y(_175_)
  );
  NAND _432_ (
    .A(_174_),
    .B(_175_),
    .Y(_003_[10])
  );
  NAND _433_ (
    .A(temp[11]),
    .B(_001_),
    .Y(_176_)
  );
  NAND _434_ (
    .A(salida[11]),
    .B(_153_),
    .Y(_177_)
  );
  NAND _435_ (
    .A(_176_),
    .B(_177_),
    .Y(_003_[11])
  );
  NAND _436_ (
    .A(temp[12]),
    .B(_001_),
    .Y(_178_)
  );
  NAND _437_ (
    .A(salida[12]),
    .B(_153_),
    .Y(_179_)
  );
  NAND _438_ (
    .A(_178_),
    .B(_179_),
    .Y(_003_[12])
  );
  NAND _439_ (
    .A(temp[13]),
    .B(_001_),
    .Y(_180_)
  );
  NAND _440_ (
    .A(salida[13]),
    .B(_153_),
    .Y(_181_)
  );
  NAND _441_ (
    .A(_180_),
    .B(_181_),
    .Y(_003_[13])
  );
  NAND _442_ (
    .A(temp[14]),
    .B(_001_),
    .Y(_182_)
  );
  NAND _443_ (
    .A(salida[14]),
    .B(_153_),
    .Y(_183_)
  );
  NAND _444_ (
    .A(_182_),
    .B(_183_),
    .Y(_003_[14])
  );
  NAND _445_ (
    .A(temp[15]),
    .B(_001_),
    .Y(_184_)
  );
  NAND _446_ (
    .A(salida[15]),
    .B(_153_),
    .Y(_185_)
  );
  NAND _447_ (
    .A(_184_),
    .B(_185_),
    .Y(_003_[15])
  );
  NAND _448_ (
    .A(temp[16]),
    .B(_001_),
    .Y(_186_)
  );
  NAND _449_ (
    .A(salida[16]),
    .B(_153_),
    .Y(_187_)
  );
  NAND _450_ (
    .A(_186_),
    .B(_187_),
    .Y(_003_[16])
  );
  NAND _451_ (
    .A(temp[17]),
    .B(_001_),
    .Y(_188_)
  );
  NAND _452_ (
    .A(salida[17]),
    .B(_153_),
    .Y(_189_)
  );
  NAND _453_ (
    .A(_188_),
    .B(_189_),
    .Y(_003_[17])
  );
  NAND _454_ (
    .A(temp[18]),
    .B(_001_),
    .Y(_190_)
  );
  NAND _455_ (
    .A(salida[18]),
    .B(_153_),
    .Y(_191_)
  );
  NAND _456_ (
    .A(_190_),
    .B(_191_),
    .Y(_003_[18])
  );
  NAND _457_ (
    .A(temp[19]),
    .B(_001_),
    .Y(_192_)
  );
  NAND _458_ (
    .A(salida[19]),
    .B(_153_),
    .Y(_193_)
  );
  NAND _459_ (
    .A(_192_),
    .B(_193_),
    .Y(_003_[19])
  );
  NAND _460_ (
    .A(temp[20]),
    .B(_001_),
    .Y(_194_)
  );
  NAND _461_ (
    .A(salida[20]),
    .B(_153_),
    .Y(_195_)
  );
  NAND _462_ (
    .A(_194_),
    .B(_195_),
    .Y(_003_[20])
  );
  NAND _463_ (
    .A(temp[21]),
    .B(_001_),
    .Y(_196_)
  );
  NAND _464_ (
    .A(salida[21]),
    .B(_153_),
    .Y(_197_)
  );
  NAND _465_ (
    .A(_196_),
    .B(_197_),
    .Y(_003_[21])
  );
  NAND _466_ (
    .A(temp[22]),
    .B(_001_),
    .Y(_198_)
  );
  NAND _467_ (
    .A(salida[22]),
    .B(_153_),
    .Y(_199_)
  );
  NAND _468_ (
    .A(_198_),
    .B(_199_),
    .Y(_003_[22])
  );
  NAND _469_ (
    .A(temp[23]),
    .B(_001_),
    .Y(_200_)
  );
  NAND _470_ (
    .A(salida[23]),
    .B(_153_),
    .Y(_201_)
  );
  NAND _471_ (
    .A(_200_),
    .B(_201_),
    .Y(_003_[23])
  );
  NAND _472_ (
    .A(temp[24]),
    .B(_001_),
    .Y(_202_)
  );
  NAND _473_ (
    .A(salida[24]),
    .B(_153_),
    .Y(_203_)
  );
  NAND _474_ (
    .A(_202_),
    .B(_203_),
    .Y(_003_[24])
  );
  NAND _475_ (
    .A(temp[25]),
    .B(_001_),
    .Y(_204_)
  );
  NAND _476_ (
    .A(salida[25]),
    .B(_153_),
    .Y(_205_)
  );
  NAND _477_ (
    .A(_204_),
    .B(_205_),
    .Y(_003_[25])
  );
  NAND _478_ (
    .A(temp[26]),
    .B(_001_),
    .Y(_206_)
  );
  NAND _479_ (
    .A(salida[26]),
    .B(_153_),
    .Y(_207_)
  );
  NAND _480_ (
    .A(_206_),
    .B(_207_),
    .Y(_003_[26])
  );
  NAND _481_ (
    .A(temp[27]),
    .B(_001_),
    .Y(_208_)
  );
  NAND _482_ (
    .A(salida[27]),
    .B(_153_),
    .Y(_209_)
  );
  NAND _483_ (
    .A(_208_),
    .B(_209_),
    .Y(_003_[27])
  );
  NAND _484_ (
    .A(temp[28]),
    .B(_001_),
    .Y(_210_)
  );
  NAND _485_ (
    .A(salida[28]),
    .B(_153_),
    .Y(_211_)
  );
  NAND _486_ (
    .A(_210_),
    .B(_211_),
    .Y(_003_[28])
  );
  NAND _487_ (
    .A(temp[29]),
    .B(_001_),
    .Y(_212_)
  );
  NAND _488_ (
    .A(salida[29]),
    .B(_153_),
    .Y(_213_)
  );
  NAND _489_ (
    .A(_212_),
    .B(_213_),
    .Y(_003_[29])
  );
  NAND _490_ (
    .A(temp[30]),
    .B(_001_),
    .Y(_214_)
  );
  NAND _491_ (
    .A(salida[30]),
    .B(_153_),
    .Y(_215_)
  );
  NAND _492_ (
    .A(_214_),
    .B(_215_),
    .Y(_003_[30])
  );
  NAND _493_ (
    .A(temp[31]),
    .B(_001_),
    .Y(_216_)
  );
  NAND _494_ (
    .A(salida[31]),
    .B(_153_),
    .Y(_217_)
  );
  NAND _495_ (
    .A(_216_),
    .B(_217_),
    .Y(_003_[31])
  );
  NOR _496_ (
    .A(selector[0]),
    .B(_057_),
    .Y(_004_[0])
  );
  NOR _497_ (
    .A(_058_),
    .B(_136_),
    .Y(_004_[1])
  );
  NOR _498_ (
    .A(_019_),
    .B(_055_),
    .Y(_002_[0])
  );
  NOR _499_ (
    .A(_020_),
    .B(_055_),
    .Y(_002_[1])
  );
  NOR _500_ (
    .A(_021_),
    .B(_055_),
    .Y(_002_[2])
  );
  NOR _501_ (
    .A(_022_),
    .B(_055_),
    .Y(_002_[3])
  );
  NOR _502_ (
    .A(_023_),
    .B(_055_),
    .Y(_002_[4])
  );
  NOR _503_ (
    .A(_024_),
    .B(_055_),
    .Y(_002_[5])
  );
  NOR _504_ (
    .A(_025_),
    .B(_055_),
    .Y(_002_[6])
  );
  NOR _505_ (
    .A(_026_),
    .B(_055_),
    .Y(_002_[7])
  );
  NOR _506_ (
    .A(_027_),
    .B(_055_),
    .Y(_002_[8])
  );
  NOR _507_ (
    .A(_028_),
    .B(_055_),
    .Y(_002_[9])
  );
  NOR _508_ (
    .A(_029_),
    .B(_055_),
    .Y(_002_[10])
  );
  NOR _509_ (
    .A(_030_),
    .B(_055_),
    .Y(_002_[11])
  );
  NOR _510_ (
    .A(_031_),
    .B(_055_),
    .Y(_002_[12])
  );
  NOR _511_ (
    .A(_032_),
    .B(_055_),
    .Y(_002_[13])
  );
  NOR _512_ (
    .A(_033_),
    .B(_055_),
    .Y(_002_[14])
  );
  NOR _513_ (
    .A(_034_),
    .B(_055_),
    .Y(_002_[15])
  );
  NOR _514_ (
    .A(_035_),
    .B(_055_),
    .Y(_002_[16])
  );
  NOR _515_ (
    .A(_036_),
    .B(_055_),
    .Y(_002_[17])
  );
  NOR _516_ (
    .A(_037_),
    .B(_055_),
    .Y(_002_[18])
  );
  NOR _517_ (
    .A(_038_),
    .B(_055_),
    .Y(_002_[19])
  );
  NOR _518_ (
    .A(_039_),
    .B(_055_),
    .Y(_002_[20])
  );
  NOR _519_ (
    .A(_040_),
    .B(_055_),
    .Y(_002_[21])
  );
  NOR _520_ (
    .A(_041_),
    .B(_055_),
    .Y(_002_[22])
  );
  NOR _521_ (
    .A(_042_),
    .B(_055_),
    .Y(_002_[23])
  );
  NOR _522_ (
    .A(_043_),
    .B(_055_),
    .Y(_002_[24])
  );
  NOR _523_ (
    .A(_044_),
    .B(_055_),
    .Y(_002_[25])
  );
  NOR _524_ (
    .A(_045_),
    .B(_055_),
    .Y(_002_[26])
  );
  NOR _525_ (
    .A(_046_),
    .B(_055_),
    .Y(_002_[27])
  );
  NOR _526_ (
    .A(_047_),
    .B(_055_),
    .Y(_002_[28])
  );
  NOR _527_ (
    .A(_048_),
    .B(_055_),
    .Y(_002_[29])
  );
  NOR _528_ (
    .A(_049_),
    .B(_055_),
    .Y(_002_[30])
  );
  NOR _529_ (
    .A(_050_),
    .B(_055_),
    .Y(_002_[31])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _530_ (
    .C(clk_4MHz),
    .D(_000_),
    .Q(endOfWord)
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _531_ (
    .C(clk_4MHz),
    .D(_001_),
    .Q(internalValid)
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _532_ (
    .C(clk_4MHz),
    .D(internalValid),
    .Q(delayedValid1)
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _533_ (
    .C(clk_4MHz),
    .D(_002_[0]),
    .Q(salidaMux_E[0])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _534_ (
    .C(clk_4MHz),
    .D(_002_[1]),
    .Q(salidaMux_E[1])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _535_ (
    .C(clk_4MHz),
    .D(_002_[2]),
    .Q(salidaMux_E[2])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _536_ (
    .C(clk_4MHz),
    .D(_002_[3]),
    .Q(salidaMux_E[3])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _537_ (
    .C(clk_4MHz),
    .D(_002_[4]),
    .Q(salidaMux_E[4])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _538_ (
    .C(clk_4MHz),
    .D(_002_[5]),
    .Q(salidaMux_E[5])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _539_ (
    .C(clk_4MHz),
    .D(_002_[6]),
    .Q(salidaMux_E[6])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _540_ (
    .C(clk_4MHz),
    .D(_002_[7]),
    .Q(salidaMux_E[7])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _541_ (
    .C(clk_4MHz),
    .D(_002_[8]),
    .Q(salidaMux_E[8])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _542_ (
    .C(clk_4MHz),
    .D(_002_[9]),
    .Q(salidaMux_E[9])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _543_ (
    .C(clk_4MHz),
    .D(_002_[10]),
    .Q(salidaMux_E[10])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _544_ (
    .C(clk_4MHz),
    .D(_002_[11]),
    .Q(salidaMux_E[11])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _545_ (
    .C(clk_4MHz),
    .D(_002_[12]),
    .Q(salidaMux_E[12])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _546_ (
    .C(clk_4MHz),
    .D(_002_[13]),
    .Q(salidaMux_E[13])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _547_ (
    .C(clk_4MHz),
    .D(_002_[14]),
    .Q(salidaMux_E[14])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _548_ (
    .C(clk_4MHz),
    .D(_002_[15]),
    .Q(salidaMux_E[15])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _549_ (
    .C(clk_4MHz),
    .D(_002_[16]),
    .Q(salidaMux_E[16])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _550_ (
    .C(clk_4MHz),
    .D(_002_[17]),
    .Q(salidaMux_E[17])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _551_ (
    .C(clk_4MHz),
    .D(_002_[18]),
    .Q(salidaMux_E[18])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _552_ (
    .C(clk_4MHz),
    .D(_002_[19]),
    .Q(salidaMux_E[19])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _553_ (
    .C(clk_4MHz),
    .D(_002_[20]),
    .Q(salidaMux_E[20])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _554_ (
    .C(clk_4MHz),
    .D(_002_[21]),
    .Q(salidaMux_E[21])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _555_ (
    .C(clk_4MHz),
    .D(_002_[22]),
    .Q(salidaMux_E[22])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _556_ (
    .C(clk_4MHz),
    .D(_002_[23]),
    .Q(salidaMux_E[23])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _557_ (
    .C(clk_4MHz),
    .D(_002_[24]),
    .Q(salidaMux_E[24])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _558_ (
    .C(clk_4MHz),
    .D(_002_[25]),
    .Q(salidaMux_E[25])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _559_ (
    .C(clk_4MHz),
    .D(_002_[26]),
    .Q(salidaMux_E[26])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _560_ (
    .C(clk_4MHz),
    .D(_002_[27]),
    .Q(salidaMux_E[27])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _561_ (
    .C(clk_4MHz),
    .D(_002_[28]),
    .Q(salidaMux_E[28])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _562_ (
    .C(clk_4MHz),
    .D(_002_[29]),
    .Q(salidaMux_E[29])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _563_ (
    .C(clk_4MHz),
    .D(_002_[30]),
    .Q(salidaMux_E[30])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _564_ (
    .C(clk_4MHz),
    .D(_002_[31]),
    .Q(salidaMux_E[31])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _565_ (
    .C(clk_4MHz),
    .D(_006_),
    .Q(validOut)
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _566_ (
    .C(clk_4MHz),
    .D(delayedValid1),
    .Q(delayedValid2)
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _567_ (
    .C(clk_4MHz),
    .D(delayedValid2),
    .Q(delayedValid3)
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _568_ (
    .C(clk_4MHz),
    .D(_004_[0]),
    .Q(selector[0])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _569_ (
    .C(clk_4MHz),
    .D(_004_[1]),
    .Q(selector[1])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _570_ (
    .C(clk_4MHz),
    .D(_005_[0]),
    .Q(temp[0])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _571_ (
    .C(clk_4MHz),
    .D(_005_[1]),
    .Q(temp[1])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _572_ (
    .C(clk_4MHz),
    .D(_005_[2]),
    .Q(temp[2])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _573_ (
    .C(clk_4MHz),
    .D(_005_[3]),
    .Q(temp[3])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _574_ (
    .C(clk_4MHz),
    .D(_005_[4]),
    .Q(temp[4])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _575_ (
    .C(clk_4MHz),
    .D(_005_[5]),
    .Q(temp[5])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _576_ (
    .C(clk_4MHz),
    .D(_005_[6]),
    .Q(temp[6])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _577_ (
    .C(clk_4MHz),
    .D(_005_[7]),
    .Q(temp[7])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _578_ (
    .C(clk_4MHz),
    .D(_005_[8]),
    .Q(temp[8])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _579_ (
    .C(clk_4MHz),
    .D(_005_[9]),
    .Q(temp[9])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _580_ (
    .C(clk_4MHz),
    .D(_005_[10]),
    .Q(temp[10])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _581_ (
    .C(clk_4MHz),
    .D(_005_[11]),
    .Q(temp[11])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _582_ (
    .C(clk_4MHz),
    .D(_005_[12]),
    .Q(temp[12])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _583_ (
    .C(clk_4MHz),
    .D(_005_[13]),
    .Q(temp[13])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _584_ (
    .C(clk_4MHz),
    .D(_005_[14]),
    .Q(temp[14])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _585_ (
    .C(clk_4MHz),
    .D(_005_[15]),
    .Q(temp[15])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _586_ (
    .C(clk_4MHz),
    .D(_005_[16]),
    .Q(temp[16])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _587_ (
    .C(clk_4MHz),
    .D(_005_[17]),
    .Q(temp[17])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _588_ (
    .C(clk_4MHz),
    .D(_005_[18]),
    .Q(temp[18])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _589_ (
    .C(clk_4MHz),
    .D(_005_[19]),
    .Q(temp[19])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _590_ (
    .C(clk_4MHz),
    .D(_005_[20]),
    .Q(temp[20])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _591_ (
    .C(clk_4MHz),
    .D(_005_[21]),
    .Q(temp[21])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _592_ (
    .C(clk_4MHz),
    .D(_005_[22]),
    .Q(temp[22])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _593_ (
    .C(clk_4MHz),
    .D(_005_[23]),
    .Q(temp[23])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _594_ (
    .C(clk_4MHz),
    .D(_005_[24]),
    .Q(temp[24])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _595_ (
    .C(clk_4MHz),
    .D(_005_[25]),
    .Q(temp[25])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _596_ (
    .C(clk_4MHz),
    .D(_005_[26]),
    .Q(temp[26])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _597_ (
    .C(clk_4MHz),
    .D(_005_[27]),
    .Q(temp[27])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _598_ (
    .C(clk_4MHz),
    .D(_005_[28]),
    .Q(temp[28])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _599_ (
    .C(clk_4MHz),
    .D(_005_[29]),
    .Q(temp[29])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _600_ (
    .C(clk_4MHz),
    .D(_005_[30]),
    .Q(temp[30])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _601_ (
    .C(clk_4MHz),
    .D(_005_[31]),
    .Q(temp[31])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _602_ (
    .C(clk_4MHz),
    .D(_003_[0]),
    .Q(salida[0])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _603_ (
    .C(clk_4MHz),
    .D(_003_[1]),
    .Q(salida[1])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _604_ (
    .C(clk_4MHz),
    .D(_003_[2]),
    .Q(salida[2])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _605_ (
    .C(clk_4MHz),
    .D(_003_[3]),
    .Q(salida[3])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _606_ (
    .C(clk_4MHz),
    .D(_003_[4]),
    .Q(salida[4])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _607_ (
    .C(clk_4MHz),
    .D(_003_[5]),
    .Q(salida[5])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _608_ (
    .C(clk_4MHz),
    .D(_003_[6]),
    .Q(salida[6])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _609_ (
    .C(clk_4MHz),
    .D(_003_[7]),
    .Q(salida[7])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _610_ (
    .C(clk_4MHz),
    .D(_003_[8]),
    .Q(salida[8])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _611_ (
    .C(clk_4MHz),
    .D(_003_[9]),
    .Q(salida[9])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _612_ (
    .C(clk_4MHz),
    .D(_003_[10]),
    .Q(salida[10])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _613_ (
    .C(clk_4MHz),
    .D(_003_[11]),
    .Q(salida[11])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _614_ (
    .C(clk_4MHz),
    .D(_003_[12]),
    .Q(salida[12])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _615_ (
    .C(clk_4MHz),
    .D(_003_[13]),
    .Q(salida[13])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _616_ (
    .C(clk_4MHz),
    .D(_003_[14]),
    .Q(salida[14])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _617_ (
    .C(clk_4MHz),
    .D(_003_[15]),
    .Q(salida[15])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _618_ (
    .C(clk_4MHz),
    .D(_003_[16]),
    .Q(salida[16])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _619_ (
    .C(clk_4MHz),
    .D(_003_[17]),
    .Q(salida[17])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _620_ (
    .C(clk_4MHz),
    .D(_003_[18]),
    .Q(salida[18])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _621_ (
    .C(clk_4MHz),
    .D(_003_[19]),
    .Q(salida[19])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _622_ (
    .C(clk_4MHz),
    .D(_003_[20]),
    .Q(salida[20])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _623_ (
    .C(clk_4MHz),
    .D(_003_[21]),
    .Q(salida[21])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _624_ (
    .C(clk_4MHz),
    .D(_003_[22]),
    .Q(salida[22])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _625_ (
    .C(clk_4MHz),
    .D(_003_[23]),
    .Q(salida[23])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _626_ (
    .C(clk_4MHz),
    .D(_003_[24]),
    .Q(salida[24])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _627_ (
    .C(clk_4MHz),
    .D(_003_[25]),
    .Q(salida[25])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _628_ (
    .C(clk_4MHz),
    .D(_003_[26]),
    .Q(salida[26])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _629_ (
    .C(clk_4MHz),
    .D(_003_[27]),
    .Q(salida[27])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _630_ (
    .C(clk_4MHz),
    .D(_003_[28]),
    .Q(salida[28])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _631_ (
    .C(clk_4MHz),
    .D(_003_[29]),
    .Q(salida[29])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _632_ (
    .C(clk_4MHz),
    .D(_003_[30]),
    .Q(salida[30])
  );
  (* src = "mux_8x32_C.v:14" *)
  DFF _633_ (
    .C(clk_4MHz),
    .D(_003_[31]),
    .Q(salida[31])
  );
endmodule
