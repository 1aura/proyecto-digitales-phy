`timescale 	1ns				/ 100ps
// escala	unidad temporal (valor de "#1") / precisión
// includes de archivos de verilog
`include "probadorMux.v"
`include "mux_8x32_C.v"
`include "mux_8x32_E.v"
`include "cmos_cells.v"


module BancoPruebas; // Testbench
	// Declarar todo como wires
	wire clk_1MHz, clk_2MHz, clk_4MHz, clk_32MHz, validOut, validIn, reset, internalValid;
	wire[31:0] salidaMux_C, salidaMux_E;
	wire[7:0] muxInput;
	
	// mux conductual
	mux_8x32_C		mux_8x32_C(/*AUTOINST*/
					   // Outputs
					   .salidaMux_C		(salidaMux_C[31:0]),
					   .validOut		(validOut),
					   // Inputs
					   .clk_4MHz		(clk_4MHz),
					   .validIn		(validIn),
					   .reset		(reset),
					   .muxInput		(muxInput[7:0]));
					   
	
	// mux estructural
	mux_8x32_E		mux_8x32_E(/*AUTOINST*/
					   // Outputs
					   .salidaMux_E		(salidaMux_E[31:0]),
					   .validOut		(validOut),
					   // Inputs
					   .clk_4MHz		(clk_4MHz),
					   .muxInput		(muxInput[7:0]),
					   .reset		(reset),
					   .validIn		(validIn));
					     

	
	// probador: describir el probador
	probadorMux		probadorMux(/*AUTOINST*/
					    // Outputs
					    .clk_1MHz		(clk_1MHz),
					    .clk_2MHz		(clk_2MHz),
					    .clk_4MHz		(clk_4MHz),
					    .clk_32MHz		(clk_32MHz),
					    .validIn		(validIn),
					    .reset		(reset),
					    .muxInput		(muxInput[7:0]),
					    // Inputs
					    .validOut		(validOut),
					    .salidaMux_C	(salidaMux_C[31:0]),
					    .salidaMux_E	(salidaMux_E[31:0]));
	
endmodule
