module dmux2(
input  clk,
input sel,
input [31:0]data_in,
input valid_int,

output reg [31:0] lane_0,
output reg [31:0] lane_1,
output reg valid_out0,
output reg valid_out1);

always@(posedge clk) begin 

  

case (sel)
0: begin
  if(valid_int==1) 
    lane_0<=data_in;
    valid_out0<=valid_int;
    repeat(1);
end

1: begin 
  if(valid_int==1) 
     lane_1<=data_in;
     valid_out1<=valid_int;
   repeat(1);
   
end
endcase
end


endmodule