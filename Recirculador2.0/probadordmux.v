module probadordmux(
  input [31:0] lane_0,
input [31:0] lane_1,
input valid_out0,
input valid_out1,
input [31:0] lane_active,
input [31:0] lane_probador,
input valid_salida0,
input valid_salida1,

output reg [31:0]datos,
output reg valid_entrada,
output reg valid_int,
output reg active0,
output reg active1,
output reg clk,
output reg reset,
output reg sel,
output reg [31:0]data_in);

initial begin
    $dumpfile("muxtotal.vcd");
    $dumpvars;
     $display ("\t\tlane_0,\tlane_1,\tclk,\tsel,\tdata_in");
    $monitor($time,"\t%b\t%b\t%b\t%b\t%b", lane_0,lane_1,clk,sel,data_in);
       
		data_in<=0;
		datos<=0;
		valid_int <= 0;
		valid_entrada <= 0;
		active0 <= 0;
		active1 <= 0;
		reset <= 0;
	@(posedge clk);

		valid_int = 1;
		data_in<=1901;
		sel<=0;
		valid_entrada <= 1;
		datos<=276;
	@(posedge clk);

		data_in<=276;
		valid_int<=1;
		sel<=1;
		valid_entrada <= 1;
		datos<=2766;

	@(posedge clk);

		data_in<=4176;
		valid_int<=1;
		sel<=0;
		reset <= 1;
		valid_entrada <= 1;
		datos<=1276;


	@(posedge clk);
		valid_int<=1;
		data_in<=2110;
		sel<=1;

		valid_entrada <= 1;
		datos<=126;


	@(posedge clk);
		valid_int<=1;
		data_in<=2760;
		sel<=0;

		valid_entrada <= 1;
		datos<=1276;

	@(posedge clk);
		valid_int<=1;
		data_in<=4000;
		sel<=1;

		valid_entrada <= 1;
		datos<=3126;



	@(posedge clk);
		valid_int<=1;
		data_in<=1230;

		sel<=0;

		valid_entrada <= 1;
		datos<=312;


	@(posedge clk);
		valid_int<=0;
		data_in<=1530;
		sel<=1;

		valid_entrada <= 1;
		datos<=3126;

	@(posedge clk);
		valid_int<=1;

		data_in<=1830;

		sel<=0;
		valid_entrada <= 1;
		datos<=316;



	@(posedge clk);
		valid_int<=1;

		data_in<=2530;
		sel<=1;

		valid_entrada <= 1;
		datos<=3476;

	@(posedge clk);

		data_in<=3823;
		sel<=0;
		valid_entrada <= 1;
		datos<=1316;

	@(posedge clk);
		valid_int<=1;

		data_in<=3370;
		sel<=1;

		valid_entrada <= 1;
		datos<=789;

	@(posedge clk);
		valid_int<=1;

		data_in<=4233;
		sel<=0;

		valid_entrada <= 1;
		datos<=316;
		@(posedge clk);
		valid_int<=1;

		data_in<=3357;
		sel<=1;
		valid_entrada <= 1;
		datos<=136;

       $finish;
            end
        initial	clk 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	    always	#26 clk 	<= ~clk;

	    always begin
		repeat(5) @(posedge clk);
			active0 <= 1;
	    end
       	    always begin
		repeat(3) @(posedge clk);
			active1 <= 1;
	    end
	    always @(posedge clk)begin;
		repeat(1) @(posedge clk);	
			valid_entrada <= ~ valid_entrada;	    
	    end
endmodule
