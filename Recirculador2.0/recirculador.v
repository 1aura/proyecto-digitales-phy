module recirculador(
input  clk,
input [31:0]datos,
input active0,
input active1,
input valid_entrada,
input reset,

output reg [31:0] lane_active,
output reg [31:0] lane_probador,
output reg valid_salida0,
output reg valid_salida1);

reg active;

	always@(posedge clk) begin

		if (reset==0)begin
			lane_active 	<= 0;
			lane_probador 	<= 0;
		end
		
		else begin

			if (active==1) begin
			  if(valid_entrada==1) 
			    lane_active<=datos;
			    valid_salida0<=valid_entrada;
			end

			if (active==0) begin
			  valid_salida0<=0;
			  if(valid_entrada==1) 
			     lane_probador<=datos;
			     valid_salida1<=valid_entrada;
			end
		end
	end

	always @(*)
		active = active0 & active1;

endmodule
