/* Generated by Yosys 0.7 (git sha1 61f6811, gcc 6.2.0-11ubuntu1 -O2 -fdebug-prefix-map=/build/yosys-OIL3SR/yosys-0.7=. -fstack-protector-strong -fPIC -Os) */

(* top =  1  *)
(* src = "recirculador.v:1" *)
module recirculador_E(clk, datos, active0, active1, valid_entrada, reset, lane_active, lane_probador, valid_salida0, valid_salida1);
  (* src = "recirculador.v:16" *)
  wire [31:0] _000_;
  (* src = "recirculador.v:16" *)
  wire [31:0] _001_;
  (* src = "recirculador.v:16" *)
  wire _002_;
  (* src = "recirculador.v:16" *)
  wire _003_;
  wire _004_;
  wire _005_;
  wire _006_;
  wire _007_;
  wire _008_;
  wire _009_;
  wire _010_;
  wire _011_;
  wire _012_;
  wire _013_;
  wire _014_;
  wire _015_;
  wire _016_;
  wire _017_;
  wire _018_;
  wire _019_;
  wire _020_;
  wire _021_;
  wire _022_;
  wire _023_;
  wire _024_;
  wire _025_;
  wire _026_;
  wire _027_;
  wire _028_;
  wire _029_;
  wire _030_;
  wire _031_;
  wire _032_;
  wire _033_;
  wire _034_;
  wire _035_;
  wire _036_;
  wire _037_;
  wire _038_;
  wire _039_;
  wire _040_;
  wire _041_;
  wire _042_;
  wire _043_;
  wire _044_;
  wire _045_;
  wire _046_;
  wire _047_;
  wire _048_;
  wire _049_;
  wire _050_;
  wire _051_;
  wire _052_;
  wire _053_;
  wire _054_;
  wire _055_;
  wire _056_;
  wire _057_;
  wire _058_;
  wire _059_;
  wire _060_;
  wire _061_;
  wire _062_;
  wire _063_;
  wire _064_;
  wire _065_;
  wire _066_;
  wire _067_;
  wire _068_;
  wire _069_;
  wire _070_;
  wire _071_;
  wire _072_;
  wire _073_;
  wire _074_;
  wire _075_;
  wire _076_;
  wire _077_;
  wire _078_;
  wire _079_;
  wire _080_;
  wire _081_;
  wire _082_;
  wire _083_;
  wire _084_;
  wire _085_;
  wire _086_;
  wire _087_;
  wire _088_;
  wire _089_;
  wire _090_;
  wire _091_;
  wire _092_;
  wire _093_;
  wire _094_;
  wire _095_;
  wire _096_;
  wire _097_;
  wire _098_;
  wire _099_;
  wire _100_;
  wire _101_;
  wire _102_;
  wire _103_;
  wire _104_;
  wire _105_;
  wire _106_;
  wire _107_;
  wire _108_;
  wire _109_;
  wire _110_;
  wire _111_;
  wire _112_;
  wire _113_;
  wire _114_;
  wire _115_;
  wire _116_;
  wire _117_;
  wire _118_;
  wire _119_;
  wire _120_;
  wire _121_;
  wire _122_;
  wire _123_;
  wire _124_;
  wire _125_;
  wire _126_;
  wire _127_;
  wire _128_;
  wire _129_;
  wire _130_;
  wire _131_;
  wire _132_;
  wire _133_;
  wire _134_;
  wire _135_;
  wire _136_;
  wire _137_;
  wire _138_;
  wire _139_;
  wire _140_;
  wire _141_;
  wire _142_;
  wire _143_;
  wire _144_;
  wire _145_;
  wire _146_;
  wire _147_;
  wire _148_;
  wire _149_;
  wire _150_;
  wire _151_;
  wire _152_;
  wire _153_;
  wire _154_;
  wire _155_;
  wire _156_;
  wire _157_;
  wire _158_;
  wire _159_;
  wire _160_;
  wire _161_;
  wire _162_;
  wire _163_;
  wire _164_;
  wire _165_;
  wire _166_;
  wire _167_;
  wire _168_;
  wire _169_;
  wire _170_;
  wire _171_;
  wire _172_;
  wire _173_;
  wire _174_;
  wire _175_;
  wire _176_;
  wire _177_;
  wire _178_;
  wire _179_;
  wire _180_;
  wire _181_;
  wire _182_;
  wire _183_;
  wire _184_;
  wire _185_;
  wire _186_;
  wire _187_;
  wire _188_;
  wire _189_;
  wire _190_;
  wire _191_;
  wire _192_;
  wire _193_;
  wire _194_;
  wire _195_;
  wire _196_;
  wire _197_;
  wire _198_;
  wire _199_;
  wire _200_;
  wire _201_;
  wire _202_;
  wire _203_;
  wire _204_;
  wire _205_;
  wire _206_;
  wire _207_;
  wire _208_;
  wire _209_;
  wire _210_;
  wire _211_;
  wire _212_;
  wire _213_;
  wire _214_;
  wire _215_;
  wire _216_;
  wire _217_;
  wire _218_;
  wire _219_;
  wire _220_;
  wire _221_;
  wire _222_;
  wire _223_;
  wire _224_;
  wire _225_;
  wire _226_;
  wire _227_;
  wire _228_;
  wire _229_;
  wire _230_;
  wire _231_;
  wire _232_;
  wire _233_;
  wire _234_;
  wire _235_;
  wire _236_;
  wire _237_;
  wire _238_;
  wire _239_;
  wire _240_;
  (* src = "recirculador.v:4" *)
  input active0;
  (* src = "recirculador.v:5" *)
  input active1;
  (* src = "recirculador.v:2" *)
  input clk;
  (* src = "recirculador.v:3" *)
  input [31:0] datos;
  (* src = "recirculador.v:9" *)
  output [31:0] lane_active;
  (* src = "recirculador.v:10" *)
  output [31:0] lane_probador;
  (* src = "recirculador.v:7" *)
  input reset;
  (* src = "recirculador.v:6" *)
  input valid_entrada;
  (* src = "recirculador.v:11" *)
  output valid_salida0;
  (* src = "recirculador.v:12" *)
  output valid_salida1;
  NOT _241_ (
    .A(datos[0]),
    .Y(_004_)
  );
  NOT _242_ (
    .A(valid_entrada),
    .Y(_005_)
  );
  NOT _243_ (
    .A(active0),
    .Y(_006_)
  );
  NOT _244_ (
    .A(active1),
    .Y(_007_)
  );
  NOR _245_ (
    .A(_007_),
    .B(_006_),
    .Y(_008_)
  );
  NOR _246_ (
    .A(_008_),
    .B(_005_),
    .Y(_009_)
  );
  NAND _247_ (
    .A(_009_),
    .B(_004_),
    .Y(_010_)
  );
  NAND _248_ (
    .A(_010_),
    .B(reset),
    .Y(_011_)
  );
  NOR _249_ (
    .A(_009_),
    .B(lane_probador[0]),
    .Y(_012_)
  );
  NOR _250_ (
    .A(_012_),
    .B(_011_),
    .Y(_001_[0])
  );
  NOT _251_ (
    .A(datos[1]),
    .Y(_013_)
  );
  NAND _252_ (
    .A(_009_),
    .B(_013_),
    .Y(_014_)
  );
  NAND _253_ (
    .A(_014_),
    .B(reset),
    .Y(_015_)
  );
  NOR _254_ (
    .A(_009_),
    .B(lane_probador[1]),
    .Y(_016_)
  );
  NOR _255_ (
    .A(_016_),
    .B(_015_),
    .Y(_001_[1])
  );
  NOT _256_ (
    .A(datos[2]),
    .Y(_017_)
  );
  NAND _257_ (
    .A(_009_),
    .B(_017_),
    .Y(_018_)
  );
  NAND _258_ (
    .A(_018_),
    .B(reset),
    .Y(_019_)
  );
  NOR _259_ (
    .A(_009_),
    .B(lane_probador[2]),
    .Y(_020_)
  );
  NOR _260_ (
    .A(_020_),
    .B(_019_),
    .Y(_001_[2])
  );
  NOT _261_ (
    .A(datos[3]),
    .Y(_021_)
  );
  NAND _262_ (
    .A(_009_),
    .B(_021_),
    .Y(_022_)
  );
  NAND _263_ (
    .A(_022_),
    .B(reset),
    .Y(_023_)
  );
  NOR _264_ (
    .A(_009_),
    .B(lane_probador[3]),
    .Y(_024_)
  );
  NOR _265_ (
    .A(_024_),
    .B(_023_),
    .Y(_001_[3])
  );
  NOT _266_ (
    .A(datos[4]),
    .Y(_025_)
  );
  NAND _267_ (
    .A(_009_),
    .B(_025_),
    .Y(_026_)
  );
  NAND _268_ (
    .A(_026_),
    .B(reset),
    .Y(_027_)
  );
  NOR _269_ (
    .A(_009_),
    .B(lane_probador[4]),
    .Y(_028_)
  );
  NOR _270_ (
    .A(_028_),
    .B(_027_),
    .Y(_001_[4])
  );
  NOT _271_ (
    .A(datos[5]),
    .Y(_029_)
  );
  NAND _272_ (
    .A(_009_),
    .B(_029_),
    .Y(_030_)
  );
  NAND _273_ (
    .A(_030_),
    .B(reset),
    .Y(_031_)
  );
  NOR _274_ (
    .A(_009_),
    .B(lane_probador[5]),
    .Y(_032_)
  );
  NOR _275_ (
    .A(_032_),
    .B(_031_),
    .Y(_001_[5])
  );
  NOT _276_ (
    .A(datos[6]),
    .Y(_033_)
  );
  NAND _277_ (
    .A(_009_),
    .B(_033_),
    .Y(_034_)
  );
  NAND _278_ (
    .A(_034_),
    .B(reset),
    .Y(_035_)
  );
  NOR _279_ (
    .A(_009_),
    .B(lane_probador[6]),
    .Y(_036_)
  );
  NOR _280_ (
    .A(_036_),
    .B(_035_),
    .Y(_001_[6])
  );
  NOT _281_ (
    .A(datos[7]),
    .Y(_037_)
  );
  NAND _282_ (
    .A(_009_),
    .B(_037_),
    .Y(_038_)
  );
  NAND _283_ (
    .A(_038_),
    .B(reset),
    .Y(_039_)
  );
  NOR _284_ (
    .A(_009_),
    .B(lane_probador[7]),
    .Y(_040_)
  );
  NOR _285_ (
    .A(_040_),
    .B(_039_),
    .Y(_001_[7])
  );
  NOT _286_ (
    .A(datos[8]),
    .Y(_041_)
  );
  NAND _287_ (
    .A(_009_),
    .B(_041_),
    .Y(_042_)
  );
  NAND _288_ (
    .A(_042_),
    .B(reset),
    .Y(_043_)
  );
  NOR _289_ (
    .A(_009_),
    .B(lane_probador[8]),
    .Y(_044_)
  );
  NOR _290_ (
    .A(_044_),
    .B(_043_),
    .Y(_001_[8])
  );
  NOT _291_ (
    .A(datos[9]),
    .Y(_045_)
  );
  NAND _292_ (
    .A(_009_),
    .B(_045_),
    .Y(_046_)
  );
  NAND _293_ (
    .A(_046_),
    .B(reset),
    .Y(_047_)
  );
  NOR _294_ (
    .A(_009_),
    .B(lane_probador[9]),
    .Y(_048_)
  );
  NOR _295_ (
    .A(_048_),
    .B(_047_),
    .Y(_001_[9])
  );
  NOT _296_ (
    .A(datos[10]),
    .Y(_049_)
  );
  NAND _297_ (
    .A(_009_),
    .B(_049_),
    .Y(_050_)
  );
  NAND _298_ (
    .A(_050_),
    .B(reset),
    .Y(_051_)
  );
  NOR _299_ (
    .A(_009_),
    .B(lane_probador[10]),
    .Y(_052_)
  );
  NOR _300_ (
    .A(_052_),
    .B(_051_),
    .Y(_001_[10])
  );
  NOT _301_ (
    .A(datos[11]),
    .Y(_053_)
  );
  NAND _302_ (
    .A(_009_),
    .B(_053_),
    .Y(_054_)
  );
  NAND _303_ (
    .A(_054_),
    .B(reset),
    .Y(_055_)
  );
  NOR _304_ (
    .A(_009_),
    .B(lane_probador[11]),
    .Y(_056_)
  );
  NOR _305_ (
    .A(_056_),
    .B(_055_),
    .Y(_001_[11])
  );
  NOT _306_ (
    .A(datos[12]),
    .Y(_057_)
  );
  NAND _307_ (
    .A(_009_),
    .B(_057_),
    .Y(_058_)
  );
  NAND _308_ (
    .A(_058_),
    .B(reset),
    .Y(_059_)
  );
  NOR _309_ (
    .A(_009_),
    .B(lane_probador[12]),
    .Y(_060_)
  );
  NOR _310_ (
    .A(_060_),
    .B(_059_),
    .Y(_001_[12])
  );
  NOT _311_ (
    .A(datos[13]),
    .Y(_061_)
  );
  NAND _312_ (
    .A(_009_),
    .B(_061_),
    .Y(_062_)
  );
  NAND _313_ (
    .A(_062_),
    .B(reset),
    .Y(_063_)
  );
  NOR _314_ (
    .A(_009_),
    .B(lane_probador[13]),
    .Y(_064_)
  );
  NOR _315_ (
    .A(_064_),
    .B(_063_),
    .Y(_001_[13])
  );
  NOT _316_ (
    .A(datos[14]),
    .Y(_065_)
  );
  NAND _317_ (
    .A(_009_),
    .B(_065_),
    .Y(_066_)
  );
  NAND _318_ (
    .A(_066_),
    .B(reset),
    .Y(_067_)
  );
  NOR _319_ (
    .A(_009_),
    .B(lane_probador[14]),
    .Y(_068_)
  );
  NOR _320_ (
    .A(_068_),
    .B(_067_),
    .Y(_001_[14])
  );
  NOT _321_ (
    .A(datos[15]),
    .Y(_069_)
  );
  NAND _322_ (
    .A(_009_),
    .B(_069_),
    .Y(_070_)
  );
  NAND _323_ (
    .A(_070_),
    .B(reset),
    .Y(_071_)
  );
  NOR _324_ (
    .A(_009_),
    .B(lane_probador[15]),
    .Y(_072_)
  );
  NOR _325_ (
    .A(_072_),
    .B(_071_),
    .Y(_001_[15])
  );
  NOT _326_ (
    .A(datos[16]),
    .Y(_073_)
  );
  NAND _327_ (
    .A(_009_),
    .B(_073_),
    .Y(_074_)
  );
  NAND _328_ (
    .A(_074_),
    .B(reset),
    .Y(_075_)
  );
  NOR _329_ (
    .A(_009_),
    .B(lane_probador[16]),
    .Y(_076_)
  );
  NOR _330_ (
    .A(_076_),
    .B(_075_),
    .Y(_001_[16])
  );
  NOT _331_ (
    .A(datos[17]),
    .Y(_077_)
  );
  NAND _332_ (
    .A(_009_),
    .B(_077_),
    .Y(_078_)
  );
  NAND _333_ (
    .A(_078_),
    .B(reset),
    .Y(_079_)
  );
  NOR _334_ (
    .A(_009_),
    .B(lane_probador[17]),
    .Y(_080_)
  );
  NOR _335_ (
    .A(_080_),
    .B(_079_),
    .Y(_001_[17])
  );
  NOT _336_ (
    .A(datos[18]),
    .Y(_081_)
  );
  NAND _337_ (
    .A(_009_),
    .B(_081_),
    .Y(_082_)
  );
  NAND _338_ (
    .A(_082_),
    .B(reset),
    .Y(_083_)
  );
  NOR _339_ (
    .A(_009_),
    .B(lane_probador[18]),
    .Y(_084_)
  );
  NOR _340_ (
    .A(_084_),
    .B(_083_),
    .Y(_001_[18])
  );
  NOT _341_ (
    .A(datos[19]),
    .Y(_085_)
  );
  NAND _342_ (
    .A(_009_),
    .B(_085_),
    .Y(_086_)
  );
  NAND _343_ (
    .A(_086_),
    .B(reset),
    .Y(_087_)
  );
  NOR _344_ (
    .A(_009_),
    .B(lane_probador[19]),
    .Y(_088_)
  );
  NOR _345_ (
    .A(_088_),
    .B(_087_),
    .Y(_001_[19])
  );
  NOT _346_ (
    .A(datos[20]),
    .Y(_089_)
  );
  NAND _347_ (
    .A(_009_),
    .B(_089_),
    .Y(_090_)
  );
  NAND _348_ (
    .A(_090_),
    .B(reset),
    .Y(_091_)
  );
  NOR _349_ (
    .A(_009_),
    .B(lane_probador[20]),
    .Y(_092_)
  );
  NOR _350_ (
    .A(_092_),
    .B(_091_),
    .Y(_001_[20])
  );
  NOT _351_ (
    .A(datos[21]),
    .Y(_093_)
  );
  NAND _352_ (
    .A(_009_),
    .B(_093_),
    .Y(_094_)
  );
  NAND _353_ (
    .A(_094_),
    .B(reset),
    .Y(_095_)
  );
  NOR _354_ (
    .A(_009_),
    .B(lane_probador[21]),
    .Y(_096_)
  );
  NOR _355_ (
    .A(_096_),
    .B(_095_),
    .Y(_001_[21])
  );
  NOT _356_ (
    .A(datos[22]),
    .Y(_097_)
  );
  NAND _357_ (
    .A(_009_),
    .B(_097_),
    .Y(_098_)
  );
  NAND _358_ (
    .A(_098_),
    .B(reset),
    .Y(_099_)
  );
  NOR _359_ (
    .A(_009_),
    .B(lane_probador[22]),
    .Y(_100_)
  );
  NOR _360_ (
    .A(_100_),
    .B(_099_),
    .Y(_001_[22])
  );
  NOT _361_ (
    .A(datos[23]),
    .Y(_101_)
  );
  NAND _362_ (
    .A(_009_),
    .B(_101_),
    .Y(_102_)
  );
  NAND _363_ (
    .A(_102_),
    .B(reset),
    .Y(_103_)
  );
  NOR _364_ (
    .A(_009_),
    .B(lane_probador[23]),
    .Y(_104_)
  );
  NOR _365_ (
    .A(_104_),
    .B(_103_),
    .Y(_001_[23])
  );
  NOT _366_ (
    .A(datos[24]),
    .Y(_105_)
  );
  NAND _367_ (
    .A(_009_),
    .B(_105_),
    .Y(_106_)
  );
  NAND _368_ (
    .A(_106_),
    .B(reset),
    .Y(_107_)
  );
  NOR _369_ (
    .A(_009_),
    .B(lane_probador[24]),
    .Y(_108_)
  );
  NOR _370_ (
    .A(_108_),
    .B(_107_),
    .Y(_001_[24])
  );
  NOT _371_ (
    .A(datos[25]),
    .Y(_109_)
  );
  NAND _372_ (
    .A(_009_),
    .B(_109_),
    .Y(_110_)
  );
  NAND _373_ (
    .A(_110_),
    .B(reset),
    .Y(_111_)
  );
  NOR _374_ (
    .A(_009_),
    .B(lane_probador[25]),
    .Y(_112_)
  );
  NOR _375_ (
    .A(_112_),
    .B(_111_),
    .Y(_001_[25])
  );
  NOT _376_ (
    .A(datos[26]),
    .Y(_113_)
  );
  NAND _377_ (
    .A(_009_),
    .B(_113_),
    .Y(_114_)
  );
  NAND _378_ (
    .A(_114_),
    .B(reset),
    .Y(_115_)
  );
  NOR _379_ (
    .A(_009_),
    .B(lane_probador[26]),
    .Y(_116_)
  );
  NOR _380_ (
    .A(_116_),
    .B(_115_),
    .Y(_001_[26])
  );
  NOT _381_ (
    .A(datos[27]),
    .Y(_117_)
  );
  NAND _382_ (
    .A(_009_),
    .B(_117_),
    .Y(_118_)
  );
  NAND _383_ (
    .A(_118_),
    .B(reset),
    .Y(_119_)
  );
  NOR _384_ (
    .A(_009_),
    .B(lane_probador[27]),
    .Y(_120_)
  );
  NOR _385_ (
    .A(_120_),
    .B(_119_),
    .Y(_001_[27])
  );
  NOT _386_ (
    .A(datos[28]),
    .Y(_121_)
  );
  NAND _387_ (
    .A(_009_),
    .B(_121_),
    .Y(_122_)
  );
  NAND _388_ (
    .A(_122_),
    .B(reset),
    .Y(_123_)
  );
  NOR _389_ (
    .A(_009_),
    .B(lane_probador[28]),
    .Y(_124_)
  );
  NOR _390_ (
    .A(_124_),
    .B(_123_),
    .Y(_001_[28])
  );
  NOT _391_ (
    .A(datos[29]),
    .Y(_125_)
  );
  NAND _392_ (
    .A(_009_),
    .B(_125_),
    .Y(_126_)
  );
  NAND _393_ (
    .A(_126_),
    .B(reset),
    .Y(_127_)
  );
  NOR _394_ (
    .A(_009_),
    .B(lane_probador[29]),
    .Y(_128_)
  );
  NOR _395_ (
    .A(_128_),
    .B(_127_),
    .Y(_001_[29])
  );
  NOT _396_ (
    .A(datos[30]),
    .Y(_129_)
  );
  NAND _397_ (
    .A(_009_),
    .B(_129_),
    .Y(_130_)
  );
  NAND _398_ (
    .A(_130_),
    .B(reset),
    .Y(_131_)
  );
  NOR _399_ (
    .A(_009_),
    .B(lane_probador[30]),
    .Y(_132_)
  );
  NOR _400_ (
    .A(_132_),
    .B(_131_),
    .Y(_001_[30])
  );
  NOT _401_ (
    .A(datos[31]),
    .Y(_133_)
  );
  NAND _402_ (
    .A(_009_),
    .B(_133_),
    .Y(_134_)
  );
  NAND _403_ (
    .A(_134_),
    .B(reset),
    .Y(_135_)
  );
  NOR _404_ (
    .A(_009_),
    .B(lane_probador[31]),
    .Y(_136_)
  );
  NOR _405_ (
    .A(_136_),
    .B(_135_),
    .Y(_001_[31])
  );
  NAND _406_ (
    .A(active1),
    .B(active0),
    .Y(_137_)
  );
  NOR _407_ (
    .A(_137_),
    .B(_005_),
    .Y(_138_)
  );
  NOR _408_ (
    .A(_138_),
    .B(lane_active[0]),
    .Y(_139_)
  );
  NAND _409_ (
    .A(_138_),
    .B(_004_),
    .Y(_140_)
  );
  NAND _410_ (
    .A(_140_),
    .B(reset),
    .Y(_141_)
  );
  NOR _411_ (
    .A(_141_),
    .B(_139_),
    .Y(_000_[0])
  );
  NOR _412_ (
    .A(_138_),
    .B(lane_active[1]),
    .Y(_142_)
  );
  NAND _413_ (
    .A(_138_),
    .B(_013_),
    .Y(_143_)
  );
  NAND _414_ (
    .A(_143_),
    .B(reset),
    .Y(_144_)
  );
  NOR _415_ (
    .A(_144_),
    .B(_142_),
    .Y(_000_[1])
  );
  NOR _416_ (
    .A(_138_),
    .B(lane_active[2]),
    .Y(_145_)
  );
  NAND _417_ (
    .A(_138_),
    .B(_017_),
    .Y(_146_)
  );
  NAND _418_ (
    .A(_146_),
    .B(reset),
    .Y(_147_)
  );
  NOR _419_ (
    .A(_147_),
    .B(_145_),
    .Y(_000_[2])
  );
  NOR _420_ (
    .A(_138_),
    .B(lane_active[3]),
    .Y(_148_)
  );
  NAND _421_ (
    .A(_138_),
    .B(_021_),
    .Y(_149_)
  );
  NAND _422_ (
    .A(_149_),
    .B(reset),
    .Y(_150_)
  );
  NOR _423_ (
    .A(_150_),
    .B(_148_),
    .Y(_000_[3])
  );
  NOR _424_ (
    .A(_138_),
    .B(lane_active[4]),
    .Y(_151_)
  );
  NAND _425_ (
    .A(_138_),
    .B(_025_),
    .Y(_152_)
  );
  NAND _426_ (
    .A(_152_),
    .B(reset),
    .Y(_153_)
  );
  NOR _427_ (
    .A(_153_),
    .B(_151_),
    .Y(_000_[4])
  );
  NOR _428_ (
    .A(_138_),
    .B(lane_active[5]),
    .Y(_154_)
  );
  NAND _429_ (
    .A(_138_),
    .B(_029_),
    .Y(_155_)
  );
  NAND _430_ (
    .A(_155_),
    .B(reset),
    .Y(_156_)
  );
  NOR _431_ (
    .A(_156_),
    .B(_154_),
    .Y(_000_[5])
  );
  NOR _432_ (
    .A(_138_),
    .B(lane_active[6]),
    .Y(_157_)
  );
  NAND _433_ (
    .A(_138_),
    .B(_033_),
    .Y(_158_)
  );
  NAND _434_ (
    .A(_158_),
    .B(reset),
    .Y(_159_)
  );
  NOR _435_ (
    .A(_159_),
    .B(_157_),
    .Y(_000_[6])
  );
  NOR _436_ (
    .A(_138_),
    .B(lane_active[7]),
    .Y(_160_)
  );
  NAND _437_ (
    .A(_138_),
    .B(_037_),
    .Y(_161_)
  );
  NAND _438_ (
    .A(_161_),
    .B(reset),
    .Y(_162_)
  );
  NOR _439_ (
    .A(_162_),
    .B(_160_),
    .Y(_000_[7])
  );
  NOR _440_ (
    .A(_138_),
    .B(lane_active[8]),
    .Y(_163_)
  );
  NAND _441_ (
    .A(_138_),
    .B(_041_),
    .Y(_164_)
  );
  NAND _442_ (
    .A(_164_),
    .B(reset),
    .Y(_165_)
  );
  NOR _443_ (
    .A(_165_),
    .B(_163_),
    .Y(_000_[8])
  );
  NOR _444_ (
    .A(_138_),
    .B(lane_active[9]),
    .Y(_166_)
  );
  NAND _445_ (
    .A(_138_),
    .B(_045_),
    .Y(_167_)
  );
  NAND _446_ (
    .A(_167_),
    .B(reset),
    .Y(_168_)
  );
  NOR _447_ (
    .A(_168_),
    .B(_166_),
    .Y(_000_[9])
  );
  NOR _448_ (
    .A(_138_),
    .B(lane_active[10]),
    .Y(_169_)
  );
  NAND _449_ (
    .A(_138_),
    .B(_049_),
    .Y(_170_)
  );
  NAND _450_ (
    .A(_170_),
    .B(reset),
    .Y(_171_)
  );
  NOR _451_ (
    .A(_171_),
    .B(_169_),
    .Y(_000_[10])
  );
  NOR _452_ (
    .A(_138_),
    .B(lane_active[11]),
    .Y(_172_)
  );
  NAND _453_ (
    .A(_138_),
    .B(_053_),
    .Y(_173_)
  );
  NAND _454_ (
    .A(_173_),
    .B(reset),
    .Y(_174_)
  );
  NOR _455_ (
    .A(_174_),
    .B(_172_),
    .Y(_000_[11])
  );
  NOR _456_ (
    .A(_138_),
    .B(lane_active[12]),
    .Y(_175_)
  );
  NAND _457_ (
    .A(_138_),
    .B(_057_),
    .Y(_176_)
  );
  NAND _458_ (
    .A(_176_),
    .B(reset),
    .Y(_177_)
  );
  NOR _459_ (
    .A(_177_),
    .B(_175_),
    .Y(_000_[12])
  );
  NOR _460_ (
    .A(_138_),
    .B(lane_active[13]),
    .Y(_178_)
  );
  NAND _461_ (
    .A(_138_),
    .B(_061_),
    .Y(_179_)
  );
  NAND _462_ (
    .A(_179_),
    .B(reset),
    .Y(_180_)
  );
  NOR _463_ (
    .A(_180_),
    .B(_178_),
    .Y(_000_[13])
  );
  NOR _464_ (
    .A(_138_),
    .B(lane_active[14]),
    .Y(_181_)
  );
  NAND _465_ (
    .A(_138_),
    .B(_065_),
    .Y(_182_)
  );
  NAND _466_ (
    .A(_182_),
    .B(reset),
    .Y(_183_)
  );
  NOR _467_ (
    .A(_183_),
    .B(_181_),
    .Y(_000_[14])
  );
  NOR _468_ (
    .A(_138_),
    .B(lane_active[15]),
    .Y(_184_)
  );
  NAND _469_ (
    .A(_138_),
    .B(_069_),
    .Y(_185_)
  );
  NAND _470_ (
    .A(_185_),
    .B(reset),
    .Y(_186_)
  );
  NOR _471_ (
    .A(_186_),
    .B(_184_),
    .Y(_000_[15])
  );
  NOR _472_ (
    .A(_138_),
    .B(lane_active[16]),
    .Y(_187_)
  );
  NAND _473_ (
    .A(_138_),
    .B(_073_),
    .Y(_188_)
  );
  NAND _474_ (
    .A(_188_),
    .B(reset),
    .Y(_189_)
  );
  NOR _475_ (
    .A(_189_),
    .B(_187_),
    .Y(_000_[16])
  );
  NOR _476_ (
    .A(_138_),
    .B(lane_active[17]),
    .Y(_190_)
  );
  NAND _477_ (
    .A(_138_),
    .B(_077_),
    .Y(_191_)
  );
  NAND _478_ (
    .A(_191_),
    .B(reset),
    .Y(_192_)
  );
  NOR _479_ (
    .A(_192_),
    .B(_190_),
    .Y(_000_[17])
  );
  NOR _480_ (
    .A(_138_),
    .B(lane_active[18]),
    .Y(_193_)
  );
  NAND _481_ (
    .A(_138_),
    .B(_081_),
    .Y(_194_)
  );
  NAND _482_ (
    .A(_194_),
    .B(reset),
    .Y(_195_)
  );
  NOR _483_ (
    .A(_195_),
    .B(_193_),
    .Y(_000_[18])
  );
  NOR _484_ (
    .A(_138_),
    .B(lane_active[19]),
    .Y(_196_)
  );
  NAND _485_ (
    .A(_138_),
    .B(_085_),
    .Y(_197_)
  );
  NAND _486_ (
    .A(_197_),
    .B(reset),
    .Y(_198_)
  );
  NOR _487_ (
    .A(_198_),
    .B(_196_),
    .Y(_000_[19])
  );
  NOR _488_ (
    .A(_138_),
    .B(lane_active[20]),
    .Y(_199_)
  );
  NAND _489_ (
    .A(_138_),
    .B(_089_),
    .Y(_200_)
  );
  NAND _490_ (
    .A(_200_),
    .B(reset),
    .Y(_201_)
  );
  NOR _491_ (
    .A(_201_),
    .B(_199_),
    .Y(_000_[20])
  );
  NOR _492_ (
    .A(_138_),
    .B(lane_active[21]),
    .Y(_202_)
  );
  NAND _493_ (
    .A(_138_),
    .B(_093_),
    .Y(_203_)
  );
  NAND _494_ (
    .A(_203_),
    .B(reset),
    .Y(_204_)
  );
  NOR _495_ (
    .A(_204_),
    .B(_202_),
    .Y(_000_[21])
  );
  NOR _496_ (
    .A(_138_),
    .B(lane_active[22]),
    .Y(_205_)
  );
  NAND _497_ (
    .A(_138_),
    .B(_097_),
    .Y(_206_)
  );
  NAND _498_ (
    .A(_206_),
    .B(reset),
    .Y(_207_)
  );
  NOR _499_ (
    .A(_207_),
    .B(_205_),
    .Y(_000_[22])
  );
  NOR _500_ (
    .A(_138_),
    .B(lane_active[23]),
    .Y(_208_)
  );
  NAND _501_ (
    .A(_138_),
    .B(_101_),
    .Y(_209_)
  );
  NAND _502_ (
    .A(_209_),
    .B(reset),
    .Y(_210_)
  );
  NOR _503_ (
    .A(_210_),
    .B(_208_),
    .Y(_000_[23])
  );
  NOR _504_ (
    .A(_138_),
    .B(lane_active[24]),
    .Y(_211_)
  );
  NAND _505_ (
    .A(_138_),
    .B(_105_),
    .Y(_212_)
  );
  NAND _506_ (
    .A(_212_),
    .B(reset),
    .Y(_213_)
  );
  NOR _507_ (
    .A(_213_),
    .B(_211_),
    .Y(_000_[24])
  );
  NOR _508_ (
    .A(_138_),
    .B(lane_active[25]),
    .Y(_214_)
  );
  NAND _509_ (
    .A(_138_),
    .B(_109_),
    .Y(_215_)
  );
  NAND _510_ (
    .A(_215_),
    .B(reset),
    .Y(_216_)
  );
  NOR _511_ (
    .A(_216_),
    .B(_214_),
    .Y(_000_[25])
  );
  NOR _512_ (
    .A(_138_),
    .B(lane_active[26]),
    .Y(_217_)
  );
  NAND _513_ (
    .A(_138_),
    .B(_113_),
    .Y(_218_)
  );
  NAND _514_ (
    .A(_218_),
    .B(reset),
    .Y(_219_)
  );
  NOR _515_ (
    .A(_219_),
    .B(_217_),
    .Y(_000_[26])
  );
  NOR _516_ (
    .A(_138_),
    .B(lane_active[27]),
    .Y(_220_)
  );
  NAND _517_ (
    .A(_138_),
    .B(_117_),
    .Y(_221_)
  );
  NAND _518_ (
    .A(_221_),
    .B(reset),
    .Y(_222_)
  );
  NOR _519_ (
    .A(_222_),
    .B(_220_),
    .Y(_000_[27])
  );
  NOR _520_ (
    .A(_138_),
    .B(lane_active[28]),
    .Y(_223_)
  );
  NAND _521_ (
    .A(_138_),
    .B(_121_),
    .Y(_224_)
  );
  NAND _522_ (
    .A(_224_),
    .B(reset),
    .Y(_225_)
  );
  NOR _523_ (
    .A(_225_),
    .B(_223_),
    .Y(_000_[28])
  );
  NOR _524_ (
    .A(_138_),
    .B(lane_active[29]),
    .Y(_226_)
  );
  NAND _525_ (
    .A(_138_),
    .B(_125_),
    .Y(_227_)
  );
  NAND _526_ (
    .A(_227_),
    .B(reset),
    .Y(_228_)
  );
  NOR _527_ (
    .A(_228_),
    .B(_226_),
    .Y(_000_[29])
  );
  NOR _528_ (
    .A(_138_),
    .B(lane_active[30]),
    .Y(_229_)
  );
  NAND _529_ (
    .A(_138_),
    .B(_129_),
    .Y(_230_)
  );
  NAND _530_ (
    .A(_230_),
    .B(reset),
    .Y(_231_)
  );
  NOR _531_ (
    .A(_231_),
    .B(_229_),
    .Y(_000_[30])
  );
  NOR _532_ (
    .A(_138_),
    .B(lane_active[31]),
    .Y(_232_)
  );
  NAND _533_ (
    .A(_138_),
    .B(_133_),
    .Y(_233_)
  );
  NAND _534_ (
    .A(_233_),
    .B(reset),
    .Y(_234_)
  );
  NOR _535_ (
    .A(_234_),
    .B(_232_),
    .Y(_000_[31])
  );
  NAND _536_ (
    .A(_009_),
    .B(reset),
    .Y(_235_)
  );
  NAND _537_ (
    .A(_137_),
    .B(reset),
    .Y(_236_)
  );
  NAND _538_ (
    .A(_236_),
    .B(valid_salida1),
    .Y(_237_)
  );
  NAND _539_ (
    .A(_237_),
    .B(_235_),
    .Y(_003_)
  );
  NAND _540_ (
    .A(_138_),
    .B(reset),
    .Y(_238_)
  );
  NOT _541_ (
    .A(reset),
    .Y(_239_)
  );
  NAND _542_ (
    .A(valid_salida0),
    .B(_239_),
    .Y(_240_)
  );
  NAND _543_ (
    .A(_240_),
    .B(_238_),
    .Y(_002_)
  );
  DFF _544_ (
    .C(clk),
    .D(_000_[0]),
    .Q(lane_active[0])
  );
  DFF _545_ (
    .C(clk),
    .D(_000_[1]),
    .Q(lane_active[1])
  );
  DFF _546_ (
    .C(clk),
    .D(_000_[2]),
    .Q(lane_active[2])
  );
  DFF _547_ (
    .C(clk),
    .D(_000_[3]),
    .Q(lane_active[3])
  );
  DFF _548_ (
    .C(clk),
    .D(_000_[4]),
    .Q(lane_active[4])
  );
  DFF _549_ (
    .C(clk),
    .D(_000_[5]),
    .Q(lane_active[5])
  );
  DFF _550_ (
    .C(clk),
    .D(_000_[6]),
    .Q(lane_active[6])
  );
  DFF _551_ (
    .C(clk),
    .D(_000_[7]),
    .Q(lane_active[7])
  );
  DFF _552_ (
    .C(clk),
    .D(_000_[8]),
    .Q(lane_active[8])
  );
  DFF _553_ (
    .C(clk),
    .D(_000_[9]),
    .Q(lane_active[9])
  );
  DFF _554_ (
    .C(clk),
    .D(_000_[10]),
    .Q(lane_active[10])
  );
  DFF _555_ (
    .C(clk),
    .D(_000_[11]),
    .Q(lane_active[11])
  );
  DFF _556_ (
    .C(clk),
    .D(_000_[12]),
    .Q(lane_active[12])
  );
  DFF _557_ (
    .C(clk),
    .D(_000_[13]),
    .Q(lane_active[13])
  );
  DFF _558_ (
    .C(clk),
    .D(_000_[14]),
    .Q(lane_active[14])
  );
  DFF _559_ (
    .C(clk),
    .D(_000_[15]),
    .Q(lane_active[15])
  );
  DFF _560_ (
    .C(clk),
    .D(_000_[16]),
    .Q(lane_active[16])
  );
  DFF _561_ (
    .C(clk),
    .D(_000_[17]),
    .Q(lane_active[17])
  );
  DFF _562_ (
    .C(clk),
    .D(_000_[18]),
    .Q(lane_active[18])
  );
  DFF _563_ (
    .C(clk),
    .D(_000_[19]),
    .Q(lane_active[19])
  );
  DFF _564_ (
    .C(clk),
    .D(_000_[20]),
    .Q(lane_active[20])
  );
  DFF _565_ (
    .C(clk),
    .D(_000_[21]),
    .Q(lane_active[21])
  );
  DFF _566_ (
    .C(clk),
    .D(_000_[22]),
    .Q(lane_active[22])
  );
  DFF _567_ (
    .C(clk),
    .D(_000_[23]),
    .Q(lane_active[23])
  );
  DFF _568_ (
    .C(clk),
    .D(_000_[24]),
    .Q(lane_active[24])
  );
  DFF _569_ (
    .C(clk),
    .D(_000_[25]),
    .Q(lane_active[25])
  );
  DFF _570_ (
    .C(clk),
    .D(_000_[26]),
    .Q(lane_active[26])
  );
  DFF _571_ (
    .C(clk),
    .D(_000_[27]),
    .Q(lane_active[27])
  );
  DFF _572_ (
    .C(clk),
    .D(_000_[28]),
    .Q(lane_active[28])
  );
  DFF _573_ (
    .C(clk),
    .D(_000_[29]),
    .Q(lane_active[29])
  );
  DFF _574_ (
    .C(clk),
    .D(_000_[30]),
    .Q(lane_active[30])
  );
  DFF _575_ (
    .C(clk),
    .D(_000_[31]),
    .Q(lane_active[31])
  );
  DFF _576_ (
    .C(clk),
    .D(_001_[0]),
    .Q(lane_probador[0])
  );
  DFF _577_ (
    .C(clk),
    .D(_001_[1]),
    .Q(lane_probador[1])
  );
  DFF _578_ (
    .C(clk),
    .D(_001_[2]),
    .Q(lane_probador[2])
  );
  DFF _579_ (
    .C(clk),
    .D(_001_[3]),
    .Q(lane_probador[3])
  );
  DFF _580_ (
    .C(clk),
    .D(_001_[4]),
    .Q(lane_probador[4])
  );
  DFF _581_ (
    .C(clk),
    .D(_001_[5]),
    .Q(lane_probador[5])
  );
  DFF _582_ (
    .C(clk),
    .D(_001_[6]),
    .Q(lane_probador[6])
  );
  DFF _583_ (
    .C(clk),
    .D(_001_[7]),
    .Q(lane_probador[7])
  );
  DFF _584_ (
    .C(clk),
    .D(_001_[8]),
    .Q(lane_probador[8])
  );
  DFF _585_ (
    .C(clk),
    .D(_001_[9]),
    .Q(lane_probador[9])
  );
  DFF _586_ (
    .C(clk),
    .D(_001_[10]),
    .Q(lane_probador[10])
  );
  DFF _587_ (
    .C(clk),
    .D(_001_[11]),
    .Q(lane_probador[11])
  );
  DFF _588_ (
    .C(clk),
    .D(_001_[12]),
    .Q(lane_probador[12])
  );
  DFF _589_ (
    .C(clk),
    .D(_001_[13]),
    .Q(lane_probador[13])
  );
  DFF _590_ (
    .C(clk),
    .D(_001_[14]),
    .Q(lane_probador[14])
  );
  DFF _591_ (
    .C(clk),
    .D(_001_[15]),
    .Q(lane_probador[15])
  );
  DFF _592_ (
    .C(clk),
    .D(_001_[16]),
    .Q(lane_probador[16])
  );
  DFF _593_ (
    .C(clk),
    .D(_001_[17]),
    .Q(lane_probador[17])
  );
  DFF _594_ (
    .C(clk),
    .D(_001_[18]),
    .Q(lane_probador[18])
  );
  DFF _595_ (
    .C(clk),
    .D(_001_[19]),
    .Q(lane_probador[19])
  );
  DFF _596_ (
    .C(clk),
    .D(_001_[20]),
    .Q(lane_probador[20])
  );
  DFF _597_ (
    .C(clk),
    .D(_001_[21]),
    .Q(lane_probador[21])
  );
  DFF _598_ (
    .C(clk),
    .D(_001_[22]),
    .Q(lane_probador[22])
  );
  DFF _599_ (
    .C(clk),
    .D(_001_[23]),
    .Q(lane_probador[23])
  );
  DFF _600_ (
    .C(clk),
    .D(_001_[24]),
    .Q(lane_probador[24])
  );
  DFF _601_ (
    .C(clk),
    .D(_001_[25]),
    .Q(lane_probador[25])
  );
  DFF _602_ (
    .C(clk),
    .D(_001_[26]),
    .Q(lane_probador[26])
  );
  DFF _603_ (
    .C(clk),
    .D(_001_[27]),
    .Q(lane_probador[27])
  );
  DFF _604_ (
    .C(clk),
    .D(_001_[28]),
    .Q(lane_probador[28])
  );
  DFF _605_ (
    .C(clk),
    .D(_001_[29]),
    .Q(lane_probador[29])
  );
  DFF _606_ (
    .C(clk),
    .D(_001_[30]),
    .Q(lane_probador[30])
  );
  DFF _607_ (
    .C(clk),
    .D(_001_[31]),
    .Q(lane_probador[31])
  );
  DFF _608_ (
    .C(clk),
    .D(_002_),
    .Q(valid_salida0)
  );
  DFF _609_ (
    .C(clk),
    .D(_003_),
    .Q(valid_salida1)
  );
endmodule
