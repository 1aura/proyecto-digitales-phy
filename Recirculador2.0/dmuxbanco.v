`timescale 	1ns				/ 100ps
`include "probadordmux.v"
`include"dmux2.v"
`include "cmos_cells.v"
`include "recirculador.v"
`include "recirculador_E.v"


module dmuxbanco;
wire  clk;
wire sel;
wire [31:0] data_in;
wire  [31:0] lane_0;
wire  [31:0] lane_1;

wire valid_int;
wire valid_out0;
wire valid_out1;

wire [31:0]datos;
wire active;
wire valid_entrada;
wire [31:0] lane_active;
wire [31:0] lane_probador;
wire valid_salida0;
wire valid_salida1;



dmux2 demux_m( /*AUTOINST*/
	      // Outputs
	      .lane_0			(lane_0[31:0]),
	      .lane_1			(lane_1[31:0]),
	      .valid_out0		(valid_out0),
	      .valid_out1		(valid_out1),
	      // Inputs
	      .clk			(clk),
	      .sel			(sel),
	      .data_in			(data_in[31:0]),
	      .valid_int		(valid_int));

recirculador recirculador_m( /*AUTOINST*/
			    // Outputs
			    .lane_active	(lane_active[31:0]),
			    .lane_probador	(lane_probador[31:0]),
			    .valid_salida0	(valid_salida0),
			    .valid_salida1	(valid_salida1),
			    // Inputs
			    .clk		(clk),
			    .datos		(datos[31:0]),
			    .active0		(active0),
			    .active1		(active1),
			    .valid_entrada	(valid_entrada),
			    .reset		(reset));
recirculador_E recirculador_E1 ( /*AUTOINST*/
				// Outputs
				.lane_active	(lane_active[31:0]),
				.lane_probador	(lane_probador[31:0]),
				.valid_salida0	(valid_salida0),
				.valid_salida1	(valid_salida1),
				// Inputs
				.active0	(active0),
				.active1	(active1),
				.clk		(clk),
				.datos		(datos[31:0]),
				.reset		(reset),
				.valid_entrada	(valid_entrada));

probadordmux probabor_m(/*AUTOINST*/
			// Outputs
			.datos		(datos[31:0]),
			.valid_entrada	(valid_entrada),
			.valid_int	(valid_int),
			.active0	(active0),
			.active1	(active1),
			.clk		(clk),
			.reset		(reset),
			.sel		(sel),
			.data_in	(data_in[31:0]),
			// Inputs
			.lane_0		(lane_0[31:0]),
			.lane_1		(lane_1[31:0]),
			.valid_out0	(valid_out0),
			.valid_out1	(valid_out1),
			.lane_active	(lane_active[31:0]),
			.lane_probador	(lane_probador[31:0]),
			.valid_salida0	(valid_salida0),
			.valid_salida1	(valid_salida1));

endmodule 
