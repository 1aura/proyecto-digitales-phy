`timescale 	1ns				/ 100ps
`include "probadordmux.v"
`include"dmux2.v"
`include "dmux2_E.v"
`include  "cmos_cells.v"




module dmuxbanco;
wire  clk_2MHz;
wire [31:0] data_in;
wire  reset_L;

wire  [31:0] lane_0;
wire  [31:0] lane_1;
wire valid_int;
wire valid_out0;
wire valid_out1;




wire  [31:0] lane_0estr;
wire  [31:0] lane_1estr;
wire valid_out0estr;
wire valid_out1estr;


dmux2 demux_C( /*AUTOINST*/
	      // Outputs
	      .lane_0			(lane_0[31:0]),
	      .lane_1			(lane_1[31:0]),
	      .valid_out0		(valid_out0),
	      .valid_out1		(valid_out1),
	      // Inputs
	      .clk_2MHz			(clk_2MHz),
	      .data_in			(data_in[31:0]),
	      .valid_int		(valid_int),
	      .reset_L			(reset_L));
dmux2_E demux_E( /*AUTOINST*/
		 // Outputs
		 .lane_0estr		(lane_0estr[31:0]),
		 .lane_1estr		(lane_1estr[31:0]),
		 .valid_out0estr	(valid_out0estr),
		 .valid_out1estr	(valid_out1estr),
		 // Inputs
		 .clk_2MHz		(clk_2MHz),
		 .data_in		(data_in[31:0]),
		 .reset_L		(reset_L),
		 .valid_int		(valid_int));


probadordmux probabor_m(/*AUTOINST*/
			// Outputs
			.reset_L	(reset_L),
			.valid_int	(valid_int),
			.clk_2MHz	(clk_2MHz),
			.data_in	(data_in[31:0]),
			// Inputs
			.lane_0		(lane_0[31:0]),
			.lane_1		(lane_1[31:0]),
			.valid_out0	(valid_out0),
			.valid_out1	(valid_out1),
			.valid_out0estr	(valid_out0estr),
			.valid_out1estr	(valid_out1estr),
			.lane_0estr	(lane_0estr[31:0]),
			.lane_1estr	(lane_1estr[31:0]));

endmodule 
