module probadordmux(
input [31:0] lane_0,
input [31:0] lane_1,
input valid_out0,
input valid_out1,
input valid_out0estr,
input valid_out1estr,
input [31:0]lane_0estr,
input [31:0]lane_1estr,
output reg reset_L,
output reg valid_int,
output reg  clk_2MHz,
output reg [31:0]data_in);

initial begin
    $dumpfile("muxtotal.vcd");
    $dumpvars;
    
       data_in=0;
       valid_int = 0;
       reset_L =0;
   
         @(posedge clk_2MHz);
       
        valid_int = 1;
        data_in<=data_in;
       
         reset_L <= 0;
       @(posedge clk_2MHz);
        
        data_in<=276;
         valid_int<=1;
       

   
          reset_L <= 0;
       @(posedge clk_2MHz);
       
        data_in<=4176;
         valid_int<=0;
       
      
    
      reset_L <= 1;
         
      @(posedge clk_2MHz);
    valid_int<=1;
        data_in<=2110;
     
        
     
          
       @(posedge clk_2MHz);
    valid_int<=1;
        data_in<=2760;
       

     @(posedge clk_2MHz);
        valid_int<=1;
        data_in<=4000;
        


         @(posedge clk_2MHz);
        valid_int<=1;
        data_in<=1230;

       
     

         @(posedge clk_2MHz);
           valid_int<=0;
        data_in<=1530;
      
       
   
       @(posedge clk_2MHz);
           valid_int<=0;

        data_in<=1830;

       

        
       
        @(posedge clk_2MHz);
           valid_int<=1;

        data_in<=25 ;

        
        @(posedge clk_2MHz);

        data_in<=3823;
      
  @(posedge clk_2MHz);
           valid_int<=1;

        data_in<=3370;
       
      

        @(posedge clk_2MHz);
           valid_int<=1;

        data_in<=4233;
       

         @(posedge clk_2MHz);
           valid_int<=1;

        data_in<=3357;
      
          
     
       $finish;
            end
        initial	clk_2MHz 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	    always	#2 clk_2MHz 	<= ~clk_2MHz;
       
   

endmodule
