module probador( // M�dulo probador: generador de se�ales y monitor de datos
	output reg				clk_1MHz, //clock es especial
	output reg 				clk_4MHz,
	output reg				clk_32MHz,
	output reg				[31:0] lineIN0,
	output reg				serial_data,
	output reg				valid0,
	output reg				valid1,
	output reg				reset);


	initial begin
//Segmento para dumpear variables//
		$dumpfile("Curvas.vcd");	// Nombre de archivo del "dump"
		$dumpvars;			// Directiva para "dumpear" variables

		$display ("\t\t\tlineIN0");
		// Mensaje que se imprime en consola cada vez que un elemento de la lista cambia
		$monitor($time,"\t%b\t\t", lineIN0);
		
		serial_data = 0;
		lineIN0 = 32'hDCBADCBA; 	// A los 3 bits, asigna un tipo bit con valor 0.

		repeat (4) @(posedge clk_1MHz);//Mantiene la simulacion por 4 flancos positivos de nuestro clock base			
		@(posedge clk_32MHz);	
		{lineIN0, lineIN0} <= 32'h00000000;	
		$finish;			// Termina de almacenar se�ales
		//end
	end
//////////////////////////////////


////Segmento para envio de datos////

	initial begin
		repeat (1) @(posedge clk_32MHz);
		repeat(4)begin//Se inicia el envio de los BC
			repeat (2)begin
				@(posedge clk_32MHz)
				serial_data <= 0;	
			end
			repeat (4)begin
				@(posedge clk_32MHz)
				serial_data <= 1;	
			end
			@(posedge clk_32MHz) begin
				serial_data <= 0;	
			end
			@(posedge clk_32MHz) begin
				serial_data <= 1;	
			end
		end

		repeat(4)begin//Se inicia el envio de los BC
			repeat (2)begin
				@(posedge clk_32MHz)
				serial_data <= 0;	
			end
			repeat (4)begin
				@(posedge clk_32MHz)
				serial_data <= 1;	
			end
			@(posedge clk_32MHz) begin
				serial_data <= 0;	
			end
			@(posedge clk_32MHz) begin
				serial_data <= 1;	
			end
		end
		
	end


//Inicializacion en 0 de las seniales de control
	initial	begin
		clk_1MHz	<= 0;			
		clk_4MHz	<= 0;			
		clk_32MHz	<= 0;			
		valid0 		<= 0;
		valid1 		<= 0;
		reset 		<= 0;		
	end


//Segmento de relojes a diferentes frecuencias//
always begin
		#1000 clk_1MHz 	<= ~clk_1MHz;	// Hace "toggle" cada 1000ns
	end

	always begin
		#250 clk_4MHz 	<= ~clk_4MHz;	// Hace "toggle" cada 250ns
	end
	always begin
		#31.25 clk_32MHz <= ~clk_32MHz;	// Hace "toggle" cada 31.25ns
	end
////////////////////////////////////////////////



////////Segmento de Valids y reset////////

	always @(posedge clk_1MHz) begin;				
		repeat(5) @(posedge clk_1MHz);				
			valid1 	<= 1;
	end


	always @(posedge clk_1MHz) begin;				
		repeat(3) @(posedge clk_1MHz);				
			valid0 	<= 1;	
	end


	always @(posedge clk_32MHz)begin
		repeat(2) @(posedge clk_32MHz);	
			reset=1;						
	end
//////////////////////////////////////////

endmodule

