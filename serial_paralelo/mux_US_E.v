/* Generated by Yosys 0.7 (git sha1 61f6811, gcc 6.2.0-11ubuntu1 -O2 -fdebug-prefix-map=/build/yosys-OIL3SR/yosys-0.7=. -fstack-protector-strong -fPIC -Os) */

(* top =  1  *)
(* src = "Serial_Paralelo.v:1" *)
module Serial_Paralelo(clk_32MHz, clk_4MHz, reset, serial_data, valid_out, serial_ParaleloOutput_C);
  (* src = "Serial_Paralelo.v:15" *)
  wire [7:0] _000_;
  (* src = "Serial_Paralelo.v:15" *)
  wire _001_;
  (* src = "Serial_Paralelo.v:15" *)
  wire [7:0] _002_;
  (* src = "Serial_Paralelo.v:15" *)
  wire [7:0] _003_;
  (* src = "Serial_Paralelo.v:15" *)
  wire [7:0] _004_;
  wire _005_;
  wire _006_;
  wire _007_;
  wire _008_;
  wire _009_;
  wire _010_;
  wire _011_;
  wire _012_;
  wire _013_;
  wire _014_;
  wire _015_;
  wire _016_;
  wire _017_;
  wire _018_;
  wire _019_;
  wire _020_;
  wire _021_;
  wire _022_;
  wire _023_;
  wire _024_;
  wire _025_;
  wire _026_;
  wire _027_;
  wire _028_;
  wire _029_;
  wire _030_;
  wire _031_;
  wire _032_;
  wire _033_;
  wire _034_;
  wire _035_;
  wire _036_;
  wire _037_;
  wire _038_;
  wire _039_;
  wire _040_;
  wire _041_;
  wire _042_;
  wire _043_;
  wire _044_;
  wire _045_;
  wire _046_;
  wire _047_;
  wire _048_;
  wire _049_;
  wire _050_;
  wire _051_;
  wire _052_;
  wire _053_;
  wire _054_;
  wire _055_;
  wire _056_;
  wire _057_;
  wire _058_;
  wire _059_;
  wire _060_;
  wire _061_;
  wire _062_;
  wire _063_;
  wire _064_;
  wire _065_;
  wire _066_;
  wire _067_;
  wire _068_;
  wire _069_;
  wire _070_;
  wire _071_;
  wire _072_;
  wire _073_;
  wire _074_;
  wire _075_;
  wire _076_;
  wire _077_;
  wire _078_;
  wire _079_;
  wire _080_;
  wire _081_;
  wire _082_;
  wire _083_;
  wire _084_;
  wire _085_;
  wire _086_;
  wire _087_;
  wire _088_;
  wire _089_;
  wire _090_;
  wire _091_;
  wire _092_;
  wire _093_;
  wire _094_;
  wire _095_;
  wire _096_;
  wire _097_;
  wire _098_;
  wire _099_;
  wire _100_;
  wire _101_;
  wire _102_;
  wire _103_;
  wire _104_;
  wire _105_;
  wire _106_;
  wire _107_;
  wire _108_;
  wire _109_;
  wire _110_;
  wire _111_;
  wire _112_;
  wire _113_;
  wire _114_;
  wire _115_;
  wire _116_;
  wire _117_;
  wire _118_;
  wire _119_;
  wire _120_;
  wire _121_;
  wire _122_;
  wire _123_;
  wire _124_;
  wire _125_;
  wire _126_;
  wire _127_;
  wire _128_;
  wire _129_;
  wire _130_;
  wire _131_;
  wire _132_;
  wire _133_;
  wire _134_;
  wire _135_;
  wire _136_;
  wire _137_;
  wire _138_;
  wire _139_;
  wire _140_;
  wire _141_;
  wire _142_;
  wire _143_;
  wire _144_;
  wire _145_;
  wire _146_;
  wire _147_;
  wire _148_;
  wire _149_;
  wire _150_;
  wire _151_;
  wire _152_;
  wire _153_;
  wire _154_;
  wire _155_;
  wire _156_;
  wire _157_;
  wire _158_;
  wire _159_;
  wire _160_;
  wire _161_;
  wire _162_;
  wire _163_;
  wire _164_;
  wire _165_;
  wire _166_;
  wire _167_;
  wire _168_;
  wire _169_;
  wire _170_;
  wire _171_;
  wire _172_;
  wire _173_;
  wire _174_;
  wire _175_;
  wire _176_;
  wire _177_;
  wire _178_;
  wire _179_;
  wire _180_;
  (* src = "Serial_Paralelo.v:10" *)
  wire [7:0] BC;
  (* src = "Serial_Paralelo.v:9" *)
  wire active;
  (* src = "Serial_Paralelo.v:2" *)
  input clk_32MHz;
  (* src = "Serial_Paralelo.v:3" *)
  input clk_4MHz;
  (* src = "Serial_Paralelo.v:4" *)
  input reset;
  (* src = "Serial_Paralelo.v:12" *)
  wire [7:0] selector;
  (* src = "Serial_Paralelo.v:7" *)
  output [7:0] serial_ParaleloOutput_C;
  (* src = "Serial_Paralelo.v:5" *)
  input serial_data;
  (* src = "Serial_Paralelo.v:11" *)
  wire [7:0] tempy;
  (* src = "Serial_Paralelo.v:6" *)
  output valid_out;
  NAND _181_ (
    .A(selector[1]),
    .B(selector[0]),
    .Y(_153_)
  );
  NOT _182_ (
    .A(selector[3]),
    .Y(_154_)
  );
  NOT _183_ (
    .A(selector[4]),
    .Y(_155_)
  );
  NAND _184_ (
    .A(_155_),
    .B(_154_),
    .Y(_156_)
  );
  NOT _185_ (
    .A(selector[7]),
    .Y(_157_)
  );
  NOR _186_ (
    .A(selector[6]),
    .B(selector[5]),
    .Y(_158_)
  );
  NAND _187_ (
    .A(_158_),
    .B(_157_),
    .Y(_159_)
  );
  NOR _188_ (
    .A(_159_),
    .B(_156_),
    .Y(_160_)
  );
  NAND _189_ (
    .A(_160_),
    .B(selector[2]),
    .Y(_161_)
  );
  NOR _190_ (
    .A(_161_),
    .B(_153_),
    .Y(_162_)
  );
  NOR _191_ (
    .A(_162_),
    .B(tempy[7]),
    .Y(_163_)
  );
  NOT _192_ (
    .A(serial_data),
    .Y(_164_)
  );
  NAND _193_ (
    .A(_162_),
    .B(_164_),
    .Y(_165_)
  );
  NAND _194_ (
    .A(_165_),
    .B(reset),
    .Y(_166_)
  );
  NOR _195_ (
    .A(_166_),
    .B(_163_),
    .Y(_004_[7])
  );
  NOT _196_ (
    .A(selector[0]),
    .Y(_167_)
  );
  NAND _197_ (
    .A(selector[1]),
    .B(_167_),
    .Y(_168_)
  );
  NOR _198_ (
    .A(_168_),
    .B(_161_),
    .Y(_169_)
  );
  NOR _199_ (
    .A(_169_),
    .B(tempy[6]),
    .Y(_170_)
  );
  NAND _200_ (
    .A(_169_),
    .B(_164_),
    .Y(_171_)
  );
  NAND _201_ (
    .A(_171_),
    .B(reset),
    .Y(_172_)
  );
  NOR _202_ (
    .A(_172_),
    .B(_170_),
    .Y(_004_[6])
  );
  NOT _203_ (
    .A(selector[1]),
    .Y(_173_)
  );
  NAND _204_ (
    .A(_173_),
    .B(selector[0]),
    .Y(_174_)
  );
  NOR _205_ (
    .A(_174_),
    .B(_161_),
    .Y(_175_)
  );
  NOR _206_ (
    .A(_175_),
    .B(tempy[5]),
    .Y(_176_)
  );
  NAND _207_ (
    .A(_175_),
    .B(_164_),
    .Y(_177_)
  );
  NAND _208_ (
    .A(_177_),
    .B(reset),
    .Y(_178_)
  );
  NOR _209_ (
    .A(_178_),
    .B(_176_),
    .Y(_004_[5])
  );
  NOR _210_ (
    .A(selector[1]),
    .B(selector[0]),
    .Y(_179_)
  );
  NOT _211_ (
    .A(_179_),
    .Y(_180_)
  );
  NOR _212_ (
    .A(_180_),
    .B(_161_),
    .Y(_005_)
  );
  NOR _213_ (
    .A(_005_),
    .B(tempy[4]),
    .Y(_006_)
  );
  NAND _214_ (
    .A(_005_),
    .B(_164_),
    .Y(_007_)
  );
  NAND _215_ (
    .A(_007_),
    .B(reset),
    .Y(_008_)
  );
  NOR _216_ (
    .A(_008_),
    .B(_006_),
    .Y(_004_[4])
  );
  NOT _217_ (
    .A(selector[2]),
    .Y(_009_)
  );
  NAND _218_ (
    .A(_160_),
    .B(_009_),
    .Y(_010_)
  );
  NOR _219_ (
    .A(_010_),
    .B(_153_),
    .Y(_011_)
  );
  NOR _220_ (
    .A(_011_),
    .B(tempy[3]),
    .Y(_012_)
  );
  NAND _221_ (
    .A(_011_),
    .B(_164_),
    .Y(_013_)
  );
  NAND _222_ (
    .A(_013_),
    .B(reset),
    .Y(_014_)
  );
  NOR _223_ (
    .A(_014_),
    .B(_012_),
    .Y(_004_[3])
  );
  NOR _224_ (
    .A(_010_),
    .B(_168_),
    .Y(_015_)
  );
  NOR _225_ (
    .A(_015_),
    .B(tempy[2]),
    .Y(_016_)
  );
  NAND _226_ (
    .A(_015_),
    .B(_164_),
    .Y(_017_)
  );
  NAND _227_ (
    .A(_017_),
    .B(reset),
    .Y(_018_)
  );
  NOR _228_ (
    .A(_018_),
    .B(_016_),
    .Y(_004_[2])
  );
  NOR _229_ (
    .A(_010_),
    .B(_174_),
    .Y(_019_)
  );
  NOR _230_ (
    .A(_019_),
    .B(tempy[1]),
    .Y(_020_)
  );
  NAND _231_ (
    .A(_019_),
    .B(_164_),
    .Y(_021_)
  );
  NAND _232_ (
    .A(_021_),
    .B(reset),
    .Y(_022_)
  );
  NOR _233_ (
    .A(_022_),
    .B(_020_),
    .Y(_004_[1])
  );
  NOR _234_ (
    .A(_010_),
    .B(_180_),
    .Y(_023_)
  );
  NOR _235_ (
    .A(_023_),
    .B(tempy[0]),
    .Y(_024_)
  );
  NAND _236_ (
    .A(_023_),
    .B(_164_),
    .Y(_025_)
  );
  NAND _237_ (
    .A(_025_),
    .B(reset),
    .Y(_026_)
  );
  NOR _238_ (
    .A(_026_),
    .B(_024_),
    .Y(_004_[0])
  );
  NOT _239_ (
    .A(reset),
    .Y(_027_)
  );
  NOR _240_ (
    .A(_162_),
    .B(_027_),
    .Y(_028_)
  );
  NOT _241_ (
    .A(_028_),
    .Y(_029_)
  );
  NOR _242_ (
    .A(_029_),
    .B(selector[0]),
    .Y(_002_[0])
  );
  NAND _243_ (
    .A(_153_),
    .B(reset),
    .Y(_030_)
  );
  NOR _244_ (
    .A(_030_),
    .B(_179_),
    .Y(_002_[1])
  );
  NOR _245_ (
    .A(_153_),
    .B(_009_),
    .Y(_031_)
  );
  NAND _246_ (
    .A(_153_),
    .B(_009_),
    .Y(_032_)
  );
  NAND _247_ (
    .A(_032_),
    .B(reset),
    .Y(_033_)
  );
  NOR _248_ (
    .A(_033_),
    .B(_031_),
    .Y(_002_[2])
  );
  NOT _249_ (
    .A(_031_),
    .Y(_034_)
  );
  NOR _250_ (
    .A(_034_),
    .B(_154_),
    .Y(_035_)
  );
  NOT _251_ (
    .A(_035_),
    .Y(_036_)
  );
  NAND _252_ (
    .A(_034_),
    .B(_154_),
    .Y(_037_)
  );
  NAND _253_ (
    .A(_037_),
    .B(_036_),
    .Y(_038_)
  );
  NOR _254_ (
    .A(_038_),
    .B(_029_),
    .Y(_002_[3])
  );
  NAND _255_ (
    .A(_035_),
    .B(selector[4]),
    .Y(_039_)
  );
  NAND _256_ (
    .A(_036_),
    .B(_155_),
    .Y(_040_)
  );
  NAND _257_ (
    .A(_040_),
    .B(_039_),
    .Y(_041_)
  );
  NOR _258_ (
    .A(_041_),
    .B(_029_),
    .Y(_002_[4])
  );
  NOT _259_ (
    .A(selector[5]),
    .Y(_042_)
  );
  NOR _260_ (
    .A(_039_),
    .B(_042_),
    .Y(_043_)
  );
  NAND _261_ (
    .A(_039_),
    .B(_042_),
    .Y(_044_)
  );
  NAND _262_ (
    .A(_044_),
    .B(_028_),
    .Y(_045_)
  );
  NOR _263_ (
    .A(_045_),
    .B(_043_),
    .Y(_002_[5])
  );
  NOR _264_ (
    .A(_043_),
    .B(selector[6]),
    .Y(_046_)
  );
  NAND _265_ (
    .A(_043_),
    .B(selector[6]),
    .Y(_047_)
  );
  NAND _266_ (
    .A(_047_),
    .B(_028_),
    .Y(_048_)
  );
  NOR _267_ (
    .A(_048_),
    .B(_046_),
    .Y(_002_[6])
  );
  NOR _268_ (
    .A(_047_),
    .B(_157_),
    .Y(_049_)
  );
  NAND _269_ (
    .A(_047_),
    .B(_157_),
    .Y(_050_)
  );
  NAND _270_ (
    .A(_050_),
    .B(_028_),
    .Y(_051_)
  );
  NOR _271_ (
    .A(_051_),
    .B(_049_),
    .Y(_002_[7])
  );
  NOT _272_ (
    .A(BC[0]),
    .Y(_052_)
  );
  NOT _273_ (
    .A(tempy[0]),
    .Y(_053_)
  );
  NOT _274_ (
    .A(tempy[2]),
    .Y(_054_)
  );
  NOR _275_ (
    .A(tempy[1]),
    .B(_054_),
    .Y(_055_)
  );
  NAND _276_ (
    .A(_055_),
    .B(_053_),
    .Y(_056_)
  );
  NOT _277_ (
    .A(tempy[4]),
    .Y(_057_)
  );
  NOR _278_ (
    .A(_057_),
    .B(active),
    .Y(_058_)
  );
  NOT _279_ (
    .A(tempy[6]),
    .Y(_059_)
  );
  NAND _280_ (
    .A(tempy[7]),
    .B(_059_),
    .Y(_060_)
  );
  NAND _281_ (
    .A(tempy[3]),
    .B(tempy[5]),
    .Y(_061_)
  );
  NOR _282_ (
    .A(_061_),
    .B(_060_),
    .Y(_062_)
  );
  NAND _283_ (
    .A(_062_),
    .B(_058_),
    .Y(_063_)
  );
  NOR _284_ (
    .A(_063_),
    .B(_056_),
    .Y(_064_)
  );
  NAND _285_ (
    .A(_064_),
    .B(_162_),
    .Y(_065_)
  );
  NAND _286_ (
    .A(_065_),
    .B(_052_),
    .Y(_066_)
  );
  NOT _287_ (
    .A(active),
    .Y(_067_)
  );
  NOR _288_ (
    .A(BC[7]),
    .B(BC[6]),
    .Y(_068_)
  );
  NOR _289_ (
    .A(BC[5]),
    .B(BC[4]),
    .Y(_069_)
  );
  NAND _290_ (
    .A(_069_),
    .B(_068_),
    .Y(_070_)
  );
  NAND _291_ (
    .A(BC[1]),
    .B(BC[0]),
    .Y(_071_)
  );
  NOT _292_ (
    .A(_071_),
    .Y(_072_)
  );
  NOR _293_ (
    .A(BC[3]),
    .B(BC[2]),
    .Y(_073_)
  );
  NAND _294_ (
    .A(_073_),
    .B(_072_),
    .Y(_074_)
  );
  NOR _295_ (
    .A(_074_),
    .B(_070_),
    .Y(_075_)
  );
  NAND _296_ (
    .A(_075_),
    .B(_067_),
    .Y(_076_)
  );
  NAND _297_ (
    .A(_076_),
    .B(reset),
    .Y(_077_)
  );
  NOT _298_ (
    .A(_077_),
    .Y(_078_)
  );
  NAND _299_ (
    .A(_078_),
    .B(_066_),
    .Y(_079_)
  );
  NOR _300_ (
    .A(_065_),
    .B(_052_),
    .Y(_080_)
  );
  NOR _301_ (
    .A(_080_),
    .B(_079_),
    .Y(_000_[0])
  );
  NOR _302_ (
    .A(_080_),
    .B(BC[1]),
    .Y(_081_)
  );
  NAND _303_ (
    .A(_031_),
    .B(_160_),
    .Y(_082_)
  );
  NOT _304_ (
    .A(tempy[1]),
    .Y(_083_)
  );
  NAND _305_ (
    .A(_083_),
    .B(tempy[2]),
    .Y(_084_)
  );
  NOR _306_ (
    .A(_084_),
    .B(tempy[0]),
    .Y(_085_)
  );
  NOT _307_ (
    .A(tempy[5]),
    .Y(_086_)
  );
  NOR _308_ (
    .A(_086_),
    .B(active),
    .Y(_087_)
  );
  NOT _309_ (
    .A(tempy[3]),
    .Y(_088_)
  );
  NOR _310_ (
    .A(_088_),
    .B(_057_),
    .Y(_089_)
  );
  NAND _311_ (
    .A(_089_),
    .B(_087_),
    .Y(_090_)
  );
  NOR _312_ (
    .A(_090_),
    .B(_060_),
    .Y(_091_)
  );
  NAND _313_ (
    .A(_091_),
    .B(_085_),
    .Y(_092_)
  );
  NOR _314_ (
    .A(_092_),
    .B(_082_),
    .Y(_093_)
  );
  NAND _315_ (
    .A(_072_),
    .B(_093_),
    .Y(_094_)
  );
  NAND _316_ (
    .A(_094_),
    .B(_078_),
    .Y(_095_)
  );
  NOR _317_ (
    .A(_095_),
    .B(_081_),
    .Y(_000_[1])
  );
  NOR _318_ (
    .A(_071_),
    .B(_065_),
    .Y(_096_)
  );
  NOR _319_ (
    .A(_096_),
    .B(BC[2]),
    .Y(_097_)
  );
  NAND _320_ (
    .A(_096_),
    .B(BC[2]),
    .Y(_098_)
  );
  NAND _321_ (
    .A(_098_),
    .B(_078_),
    .Y(_099_)
  );
  NOR _322_ (
    .A(_099_),
    .B(_097_),
    .Y(_000_[2])
  );
  NOT _323_ (
    .A(BC[3]),
    .Y(_100_)
  );
  NAND _324_ (
    .A(_098_),
    .B(_100_),
    .Y(_101_)
  );
  NAND _325_ (
    .A(_101_),
    .B(_078_),
    .Y(_102_)
  );
  NOR _326_ (
    .A(_098_),
    .B(_100_),
    .Y(_103_)
  );
  NOR _327_ (
    .A(_103_),
    .B(_102_),
    .Y(_000_[3])
  );
  NOR _328_ (
    .A(_103_),
    .B(BC[4]),
    .Y(_104_)
  );
  NOT _329_ (
    .A(BC[4]),
    .Y(_105_)
  );
  NAND _330_ (
    .A(BC[3]),
    .B(BC[2]),
    .Y(_106_)
  );
  NOR _331_ (
    .A(_106_),
    .B(_105_),
    .Y(_107_)
  );
  NAND _332_ (
    .A(_107_),
    .B(_096_),
    .Y(_108_)
  );
  NAND _333_ (
    .A(_108_),
    .B(_078_),
    .Y(_109_)
  );
  NOR _334_ (
    .A(_109_),
    .B(_104_),
    .Y(_000_[4])
  );
  NOT _335_ (
    .A(BC[5]),
    .Y(_110_)
  );
  NAND _336_ (
    .A(_108_),
    .B(_110_),
    .Y(_111_)
  );
  NAND _337_ (
    .A(_111_),
    .B(_078_),
    .Y(_112_)
  );
  NOR _338_ (
    .A(_108_),
    .B(_110_),
    .Y(_113_)
  );
  NOR _339_ (
    .A(_113_),
    .B(_112_),
    .Y(_000_[5])
  );
  NOT _340_ (
    .A(BC[6]),
    .Y(_114_)
  );
  NOT _341_ (
    .A(_107_),
    .Y(_115_)
  );
  NOR _342_ (
    .A(_115_),
    .B(_094_),
    .Y(_116_)
  );
  NAND _343_ (
    .A(_116_),
    .B(BC[5]),
    .Y(_117_)
  );
  NAND _344_ (
    .A(_117_),
    .B(_114_),
    .Y(_118_)
  );
  NAND _345_ (
    .A(_118_),
    .B(_078_),
    .Y(_119_)
  );
  NOR _346_ (
    .A(_117_),
    .B(_114_),
    .Y(_120_)
  );
  NOR _347_ (
    .A(_120_),
    .B(_119_),
    .Y(_000_[6])
  );
  NAND _348_ (
    .A(_120_),
    .B(BC[7]),
    .Y(_121_)
  );
  NOT _349_ (
    .A(BC[7]),
    .Y(_122_)
  );
  NAND _350_ (
    .A(_113_),
    .B(BC[6]),
    .Y(_123_)
  );
  NAND _351_ (
    .A(_123_),
    .B(_122_),
    .Y(_124_)
  );
  NAND _352_ (
    .A(_124_),
    .B(_121_),
    .Y(_125_)
  );
  NOR _353_ (
    .A(_125_),
    .B(_077_),
    .Y(_000_[7])
  );
  NOR _354_ (
    .A(_075_),
    .B(active),
    .Y(_126_)
  );
  NOR _355_ (
    .A(_126_),
    .B(_027_),
    .Y(_001_)
  );
  NOR _356_ (
    .A(_082_),
    .B(_067_),
    .Y(_127_)
  );
  NOR _357_ (
    .A(_127_),
    .B(serial_ParaleloOutput_C[0]),
    .Y(_128_)
  );
  NAND _358_ (
    .A(_127_),
    .B(_053_),
    .Y(_129_)
  );
  NAND _359_ (
    .A(_129_),
    .B(reset),
    .Y(_130_)
  );
  NOR _360_ (
    .A(_130_),
    .B(_128_),
    .Y(_003_[0])
  );
  NOR _361_ (
    .A(_127_),
    .B(serial_ParaleloOutput_C[1]),
    .Y(_131_)
  );
  NAND _362_ (
    .A(_127_),
    .B(_083_),
    .Y(_132_)
  );
  NAND _363_ (
    .A(_132_),
    .B(reset),
    .Y(_133_)
  );
  NOR _364_ (
    .A(_133_),
    .B(_131_),
    .Y(_003_[1])
  );
  NOR _365_ (
    .A(_127_),
    .B(serial_ParaleloOutput_C[2]),
    .Y(_134_)
  );
  NAND _366_ (
    .A(_127_),
    .B(_054_),
    .Y(_135_)
  );
  NAND _367_ (
    .A(_135_),
    .B(reset),
    .Y(_136_)
  );
  NOR _368_ (
    .A(_136_),
    .B(_134_),
    .Y(_003_[2])
  );
  NOR _369_ (
    .A(_127_),
    .B(serial_ParaleloOutput_C[3]),
    .Y(_137_)
  );
  NAND _370_ (
    .A(_127_),
    .B(_088_),
    .Y(_138_)
  );
  NAND _371_ (
    .A(_138_),
    .B(reset),
    .Y(_139_)
  );
  NOR _372_ (
    .A(_139_),
    .B(_137_),
    .Y(_003_[3])
  );
  NOR _373_ (
    .A(_127_),
    .B(serial_ParaleloOutput_C[4]),
    .Y(_140_)
  );
  NAND _374_ (
    .A(_127_),
    .B(_057_),
    .Y(_141_)
  );
  NAND _375_ (
    .A(_141_),
    .B(reset),
    .Y(_142_)
  );
  NOR _376_ (
    .A(_142_),
    .B(_140_),
    .Y(_003_[4])
  );
  NOR _377_ (
    .A(_127_),
    .B(serial_ParaleloOutput_C[5]),
    .Y(_143_)
  );
  NAND _378_ (
    .A(_127_),
    .B(_086_),
    .Y(_144_)
  );
  NAND _379_ (
    .A(_144_),
    .B(reset),
    .Y(_145_)
  );
  NOR _380_ (
    .A(_145_),
    .B(_143_),
    .Y(_003_[5])
  );
  NOR _381_ (
    .A(_127_),
    .B(serial_ParaleloOutput_C[6]),
    .Y(_146_)
  );
  NAND _382_ (
    .A(_127_),
    .B(_059_),
    .Y(_147_)
  );
  NAND _383_ (
    .A(_147_),
    .B(reset),
    .Y(_148_)
  );
  NOR _384_ (
    .A(_148_),
    .B(_146_),
    .Y(_003_[6])
  );
  NOR _385_ (
    .A(_127_),
    .B(serial_ParaleloOutput_C[7]),
    .Y(_149_)
  );
  NOT _386_ (
    .A(tempy[7]),
    .Y(_150_)
  );
  NAND _387_ (
    .A(_127_),
    .B(_150_),
    .Y(_151_)
  );
  NAND _388_ (
    .A(_151_),
    .B(reset),
    .Y(_152_)
  );
  NOR _389_ (
    .A(_152_),
    .B(_149_),
    .Y(_003_[7])
  );
  DFF _390_ (
    .C(clk_32MHz),
    .D(_003_[0]),
    .Q(serial_ParaleloOutput_C[0])
  );
  DFF _391_ (
    .C(clk_32MHz),
    .D(_003_[1]),
    .Q(serial_ParaleloOutput_C[1])
  );
  DFF _392_ (
    .C(clk_32MHz),
    .D(_003_[2]),
    .Q(serial_ParaleloOutput_C[2])
  );
  DFF _393_ (
    .C(clk_32MHz),
    .D(_003_[3]),
    .Q(serial_ParaleloOutput_C[3])
  );
  DFF _394_ (
    .C(clk_32MHz),
    .D(_003_[4]),
    .Q(serial_ParaleloOutput_C[4])
  );
  DFF _395_ (
    .C(clk_32MHz),
    .D(_003_[5]),
    .Q(serial_ParaleloOutput_C[5])
  );
  DFF _396_ (
    .C(clk_32MHz),
    .D(_003_[6]),
    .Q(serial_ParaleloOutput_C[6])
  );
  DFF _397_ (
    .C(clk_32MHz),
    .D(_003_[7]),
    .Q(serial_ParaleloOutput_C[7])
  );
  DFF _398_ (
    .C(clk_32MHz),
    .D(_001_),
    .Q(active)
  );
  DFF _399_ (
    .C(clk_32MHz),
    .D(_000_[0]),
    .Q(BC[0])
  );
  DFF _400_ (
    .C(clk_32MHz),
    .D(_000_[1]),
    .Q(BC[1])
  );
  DFF _401_ (
    .C(clk_32MHz),
    .D(_000_[2]),
    .Q(BC[2])
  );
  DFF _402_ (
    .C(clk_32MHz),
    .D(_000_[3]),
    .Q(BC[3])
  );
  DFF _403_ (
    .C(clk_32MHz),
    .D(_000_[4]),
    .Q(BC[4])
  );
  DFF _404_ (
    .C(clk_32MHz),
    .D(_000_[5]),
    .Q(BC[5])
  );
  DFF _405_ (
    .C(clk_32MHz),
    .D(_000_[6]),
    .Q(BC[6])
  );
  DFF _406_ (
    .C(clk_32MHz),
    .D(_000_[7]),
    .Q(BC[7])
  );
  DFF _407_ (
    .C(clk_32MHz),
    .D(_004_[0]),
    .Q(tempy[0])
  );
  DFF _408_ (
    .C(clk_32MHz),
    .D(_004_[1]),
    .Q(tempy[1])
  );
  DFF _409_ (
    .C(clk_32MHz),
    .D(_004_[2]),
    .Q(tempy[2])
  );
  DFF _410_ (
    .C(clk_32MHz),
    .D(_004_[3]),
    .Q(tempy[3])
  );
  DFF _411_ (
    .C(clk_32MHz),
    .D(_004_[4]),
    .Q(tempy[4])
  );
  DFF _412_ (
    .C(clk_32MHz),
    .D(_004_[5]),
    .Q(tempy[5])
  );
  DFF _413_ (
    .C(clk_32MHz),
    .D(_004_[6]),
    .Q(tempy[6])
  );
  DFF _414_ (
    .C(clk_32MHz),
    .D(_004_[7]),
    .Q(tempy[7])
  );
  DFF _415_ (
    .C(clk_32MHz),
    .D(_002_[0]),
    .Q(selector[0])
  );
  DFF _416_ (
    .C(clk_32MHz),
    .D(_002_[1]),
    .Q(selector[1])
  );
  DFF _417_ (
    .C(clk_32MHz),
    .D(_002_[2]),
    .Q(selector[2])
  );
  DFF _418_ (
    .C(clk_32MHz),
    .D(_002_[3]),
    .Q(selector[3])
  );
  DFF _419_ (
    .C(clk_32MHz),
    .D(_002_[4]),
    .Q(selector[4])
  );
  DFF _420_ (
    .C(clk_32MHz),
    .D(_002_[5]),
    .Q(selector[5])
  );
  DFF _421_ (
    .C(clk_32MHz),
    .D(_002_[6]),
    .Q(selector[6])
  );
  DFF _422_ (
    .C(clk_32MHz),
    .D(_002_[7]),
    .Q(selector[7])
  );
  assign valid_out = 1'b0;
endmodule
