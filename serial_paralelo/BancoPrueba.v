`timescale 	1ns				/ 100ps
// escala	unidad temporal (valor de "#1") / precisi�n
// includes de archivos de verilog
// Pueden omitirse y llamarse desde el testbench
`include "cmos_cells.v"
`include "Serial_Paralelo.v"
`include "Serial_Paralelo_E.v"
`include "probador.v"


module BancoPruebas; // Testbench
	// Por lo general, las se�ales en el banco de pruebas son wires.
	// No almacenan un valor, son manejadas por otras instancias de m�dulos.
	wire [31:0] data_out, data_outE, lineIN0, lineIN1;
	wire [7:0] serial_ParaleloOutput_C,serial_ParaleloOutput_E;
	wire clk_1MHz,valid_out,valid_out_2, clk_4MHz, clk_32MHz, valid0, valid1, reset;



	Serial_Paralelo		Serial_Paralelo(/*AUTOINST*/
						// Outputs
						.valid_out	(valid_out),
						.serial_ParaleloOutput_C(serial_ParaleloOutput_C[7:0]),
						// Inputs
						.clk_32MHz	(clk_32MHz),
						.clk_4MHz	(clk_4MHz),
						.reset		(reset),
						.serial_data	(serial_data));

	Serial_Paralelo_E	Serial_Paralelo_E1(/*AUTOINST*/
						   // Outputs
						   .serial_ParaleloOutput_E(serial_ParaleloOutput_E[7:0]),
						   .valid_out_2		(valid_out_2),
						   // Inputs
						   .clk_32MHz		(clk_32MHz),
						   .clk_4MHz		(clk_4MHz),
						   .reset		(reset),
						   .serial_data		(serial_data));


	// Probador: generador de se�ales y monitor
	probador probador_(/*AUTOINST*/
			   // Outputs
			   .clk_1MHz		(clk_1MHz),
			   .clk_4MHz		(clk_4MHz),
			   .clk_32MHz		(clk_32MHz),
			   .lineIN0		(lineIN0[31:0]),
			   .serial_data		(serial_data),
			   .valid0		(valid0),
			   .valid1		(valid1),
			   .reset		(reset));
endmodule
